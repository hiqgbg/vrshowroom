﻿// Copyright (c) Valve Corporation, All rights reserved. ======================================================================================================

Shader "Valve/vr_carpaint"
{
	Properties
	{
		[Toggle( S_UNLIT )] g_bUnlit( "g_bUnlit", Int ) = 0

		_Color( "Color", Color ) = ( 1, 1, 1, 1 )
		_MainTex( "Albedo", 2D ) = "white" {}
		
		_Cutoff( "Alpha Cutoff", Range( 0.0, 1.0 ) ) = 0.5

		_Glossiness("Smoothness", Range(0.0, 1.0)) = 0.5
		_SpecColor("Specular", Color) = (0.2,0.2,0.2)
		_SpecGlossMap("Specular", 2D) = "white" {}

		g_vFlakeColor( "g_vFlakeColor", Color ) = ( 1, 1, 1, 1 )
		g_vFlakeSize( "g_vFlakeSize", Float ) = 3000.0
		g_vFlakeVariation( "g_vFlakeVariation", Float ) = 1

		g_flReflectanceMin( "g_flReflectanceMin", Range( 0.0, 1.0 ) ) = 0.0
		g_flReflectanceMax( "g_flReflectanceMax", Range( 0.0, 1.0 ) ) = 1.0
		[HideInInspector] g_flReflectanceScale( "g_flReflectanceScale", Range( 0.0, 1.0 ) ) = 1.0
		[HideInInspector] g_flReflectanceBias( "g_flReflectanceBias", Range( 0.0, 1.0 ) ) = 0.0

		[Gamma] _Metallic( "Metallic", Range( 0.0, 1.0 ) ) = 0.0
		_MetallicGlossMap( "Metallic", 2D ) = "white" {}

		_BumpScale( "Scale", Float ) = 1.0
		[Normal] _BumpMap( "Normal Map", 2D ) = "bump" {}

		_Parallax ( "Height Scale", Range ( 0.005, 0.08 ) ) = 0.02
		_ParallaxMap ( "Height Map", 2D ) = "black" {}

		_OcclusionStrength( "Strength", Range( 0.0, 1.0 ) ) = 1.0
		_OcclusionMap( "Occlusion", 2D ) = "white" {}
		_OcclusionStrengthDirectDiffuse( "StrengthDirectDiffuse", Range( 0.0, 1.0 ) ) = 1.0
		_OcclusionStrengthDirectSpecular( "StrengthDirectSpecular", Range( 0.0, 1.0 ) ) = 1.0
		_OcclusionStrengthIndirectDiffuse( "StrengthIndirectDiffuse", Range( 0.0, 1.0 ) ) = 1.0
		_OcclusionStrengthIndirectSpecular( "StrengthIndirectSpecular", Range( 0.0, 1.0 ) ) = 1.0

		g_flCubeMapScalar( "Cube Map Scalar", Range( 0.0, 2.0 ) ) = 1.0

		_EmissionColor( "Color", Color ) = ( 0, 0, 0 )
		_EmissionMap( "Emission", 2D ) = "white" {}
		
		_DetailMask( "Detail Mask", 2D ) = "white" {}

		_DetailAlbedoMap( "Detail Albedo x2", 2D ) = "grey" {}
		_DetailNormalMapScale( "Scale", Float ) = 1.0
		_DetailNormalMap( "Normal Map", 2D ) = "bump" {}

		g_tOverrideLightmap( "Override Lightmap", 2D ) = "white" {}

		[Enum(UV0,0,UV1,1)] _UVSec ( "UV Set for secondary textures", Float ) = 0

		[Toggle (E_USE_FLAKES)] g_EnableFlakes("g_EnableFlakes", Int) = 0

		[Toggle( S_RECEIVE_SHADOWS )] g_bReceiveShadows( "g_bReceiveShadows", Int ) = 1

		[Toggle( S_RENDER_BACKFACES )] g_bRenderBackfaces( "g_bRenderBackfaces", Int ) = 0

		[Toggle( S_WORLD_ALIGNED_TEXTURE )] g_bWorldAlignedTexture( "g_bWorldAlignedTexture", Int ) = 0
		g_vWorldAlignedTextureSize( "g_vWorldAlignedTextureSize", Vector ) = ( 1.0, 1.0, 1.0, 0.0 )
		g_vWorldAlignedTextureNormal( "g_vWorldAlignedTextureNormal", Vector ) = ( 0.0, 1.0, 0.0, 0.0 )
		g_vWorldAlignedTexturePosition( "g_vWorldAlignedTexturePosition", Vector ) = ( 0.0, 0.0, 0.0, 0.0 )
		[HideInInspector] g_vWorldAlignedNormalTangentU( "g_vWorldAlignedNormalTangentU", Vector ) = ( -1.0, 0.0, 0.0, 0.0)
		[HideInInspector] g_vWorldAlignedNormalTangentV( "g_vWorldAlignedNormalTangentV", Vector ) = ( 0.0, 0.0, 1.0, 0.0)

		[HideInInspector] _SpecularMode( "__specularmode", Int ) = 1
		[HideInInspector] _Cull ( "__cull", Int ) = 2

		// Blending state
		[HideInInspector] _Mode ( "__mode", Float ) = 0.0
		[HideInInspector] _SrcBlend ( "__src", Float ) = 1.0
		[HideInInspector] _DstBlend ( "__dst", Float ) = 0.0
		[HideInInspector] _ZWrite ( "__zw", Float ) = 1.0
		[HideInInspector] _FogMultiplier ( "__fogmult", Float ) = 1.0
	}

	SubShader
	{
		Tags { "RenderType" = "Opaque" "PerformanceChecks" = "False" }
		LOD 300

		//-------------------------------------------------------------------------------------------------------------------------------------------------------------
		// Base forward pass (directional light, emission, lightmaps, ...)
		//-------------------------------------------------------------------------------------------------------------------------------------------------------------
		Pass
		{
			Name "FORWARD"
			Tags { "LightMode" = "ForwardBase" "PassFlags" = "OnlyDirectional" } // NOTE: "OnlyDirectional" prevents Unity from baking dynamic lights into SH terms at runtime

			Blend [_SrcBlend] [_DstBlend]
			ZWrite [_ZWrite]
			Cull [_Cull]

			CGPROGRAM
				#pragma target 5.0
				#pragma only_renderers d3d11
				#pragma exclude_renderers gles

				//-------------------------------------------------------------------------------------------------------------------------------------------------------------
				#pragma shader_feature _ _ALPHATEST_ON _ALPHABLEND_ON _ALPHAPREMULTIPLY_ON
				#pragma shader_feature _NORMALMAP
				#pragma shader_feature _METALLICGLOSSMAP
				#pragma shader_feature _SPECGLOSSMAP
				#pragma shader_feature _EMISSION
				#pragma shader_feature _DETAIL_MULX2
				//#pragma shader_feature _PARALLAXMAP

				#pragma shader_feature S_SPECULAR_NONE S_SPECULAR_BLINNPHONG S_SPECULAR_METALLIC
				#pragma shader_feature S_UNLIT
				#pragma shader_feature S_OVERRIDE_LIGHTMAP
				#pragma shader_feature S_WORLD_ALIGNED_TEXTURE
				#pragma shader_feature S_RECEIVE_SHADOWS
				#pragma shader_feature S_OCCLUSION
				#pragma shader_feature S_RENDER_BACKFACES

				#pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
				#pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
				#pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON

				#pragma multi_compile _ MATRIX_PALETTE_SKINNING_1BONE
				#pragma multi_compile _ D_VALVE_FOG
				#pragma multi_compile _ D_VALVE_SHADOWING_POINT_LIGHTS

				#pragma multi_compile _ E_USE_FLAKES

				#pragma skip_variants SHADOWS_SOFT

				#pragma vertex MainVs
				#pragma fragment MainPs

				// Dynamic combo skips (Static combo skips happen in ValveShaderGUI.cs in SetMaterialKeywords())
				#if ( S_UNLIT )
					#undef LIGHTMAP_OFF
					#define LIGHTMAP_OFF 1
					#undef LIGHTMAP_ON

					#undef DIRLIGHTMAP_OFF
					#define DIRLIGHTMAP_OFF 1
					#undef DIRLIGHTMAP_COMBINED
					#undef DIRLIGHTMAP_SEPARATE

					#undef DYNAMICLIGHTMAP_OFF
					#define DYNAMICLIGHTMAP_OFF 1
					#undef DYNAMICLIGHTMAP_ON
				#endif

				// Includes -------------------------------------------------------------------------------------------------------------------------------------------------
				#include "UnityCG.cginc"
				#include "UnityLightingCommon.cginc"
				#include "UnityStandardUtils.cginc"
				#include "UnityStandardInput.cginc"
				#include "vr_utils.cginc"
				#include "vr_lighting.cginc"
				#include "vr_matrix_palette_skinning.cginc"
				#include "vr_fog.cginc"

				// Structs --------------------------------------------------------------------------------------------------------------------------------------------------
				struct VS_INPUT
				{
					float4 vPositionOs : POSITION;
					float3 vNormalOs : NORMAL;
					float2 vTexCoord0 : TEXCOORD0;
					#if ( _DETAIL || S_OVERRIDE_LIGHTMAP || LIGHTMAP_ON )
						float2 vTexCoord1 : TEXCOORD1;
					#endif
					#if ( DYNAMICLIGHTMAP_ON || UNITY_PASS_META )
						float2 vTexCoord2 : TEXCOORD2;
					#endif

					#if ( _NORMALMAP )
						float4 vTangentUOs_flTangentVSign : TANGENT;
					#endif

					#if ( MATRIX_PALETTE_SKINNING )
						float4 vBoneIndices : COLOR;
					#endif
				};

				struct PS_INPUT
				{
					float4 vPositionPs : SV_Position;

					#if ( !S_UNLIT )
						//float3 vPositionWs : TEXCOORD0;
						//float3 vNormalWs : TEXCOORD1;
					#endif
					float3 vPositionWs : TEXCOORD0;
					float3 vNormalWs : TEXCOORD1;

					#if ( _DETAIL )
						float4 vTextureCoords : TEXCOORD2;
					#else
						float2 vTextureCoords : TEXCOORD2;
					#endif

					#if ( S_OVERRIDE_LIGHTMAP || LIGHTMAP_ON || DYNAMICLIGHTMAP_ON )
						#if ( DYNAMICLIGHTMAP_ON )
							centroid float4 vLightmapUV : TEXCOORD3;
						#else
							centroid float2 vLightmapUV : TEXCOORD3;
						#endif
					#endif

					#if ( _NORMALMAP )
						float3 vTangentUWs : TEXCOORD4;
						float3 vTangentVWs : TEXCOORD5;
					#endif

					#if ( D_VALVE_FOG )
						float2 vFogCoords : TEXCOORD6;
					#endif
				};

				/*
				float2 hash( float2 p ) { p=float2(dot(p,float2(127.1,311.7)),dot(p,float2(269.5,183.3))); return frac(sin(p)*18.5453); }
				float3 voronoi( in float2 x )
				{
    				float2 n = floor( x );
    				float2 f = frac( x );

					float3 m = float3( 8.0, 8.0, 8.0 );
    				for( int j=-1; j<=1; j++ )
    				for( int i=-1; i<=1; i++ )
    				{
    					float2  g = float2( float(i), float(j) );
    					float2  o = hash( n + g );
    					float2  r = g - f + o;
    					float d = dot( r, r );
    					if( d<m.x )
    						m = float3( d, o );
    				}

    				return normalize(float3( m.y,m.z,1.0 ));
    			}
    			*/
    			/*
    			float rand(float2 co){
  					return frac(sin(dot(co.xy, float2(12.9898,78.233))) * 43758.5453);
				}
				float2 rand3(float3 co) {
    				return float2(rand(co.xy * float2(1.0, 0.5)), rand(co.yz * float2(1.0, 0.5) + float2(0.0, 0.5)));
				}
				float3 randNormal2(float2 co) {
    				//return normalize(float3(rand2(co*2500.0), 1.0));
    				return normalize(float3(rand3(float3(co, co.x)), 1.0));
				}
				float3 randNormal3(float3 co) {
    				return normalize(float3(rand3(co), 1.0));
				}
				*/

				float mod289(float x)     { return x - floor(x * (1.0 / 1289.0)) * 1289.0; }
				float permute(float x)    { return mod289(((x*34.0)+1.0)*x); }
				float rng(float x)        { return frac(x*1.0/41.0); }
				
				// minimal example: take a 2D coordinate and convert into a hash value, then
				// generate multiple random numbers by rehashing the hash.
				float3 randNormal(float2 co)
				{
					//co = (co + float2(0.5,0.5))*float2(11,11);
  					float h;
  					float3 r;
  					float2 m = float2(mod289(co.x), mod289(co.y)); // values must be bound to (-289,289) for precision

  					h = permute(permute(m.x)+m.y);	// hash the coordinates together
  					r.x = 2.0 * (rng(h)-0.5); 		// first random number
  					h = permute(h); 				// hash the hash
  					r.y = 2.0 * (rng(h)-0.5); 		// second random number

  					//h = permute(h); 				// hash the hash
  					//r.z = rng(h);
  					r.z = 1.0;

  					return normalize(r);
				}

				float g_flValveGlobalVertexScale = 1.0; // Used to "hide" all valve materials for debugging

				// World-aligned texture
				float3 g_vWorldAlignedTextureSize = float3( 1.0, 1.0, 1.0 );
				float3 g_vWorldAlignedNormalTangentU = float3( -1.0, 0.0, 0.0 );
				float3 g_vWorldAlignedNormalTangentV = float3( 0.0, 0.0, 1.0 );
				float3 g_vWorldAlignedTexturePosition = float3( 0.0, 0.0, 0.0 );

				// MainVs ---------------------------------------------------------------------------------------------------------------------------------------------------
				PS_INPUT MainVs( VS_INPUT i )
				{
					PS_INPUT o = ( PS_INPUT )0;

					#if ( MATRIX_PALETTE_SKINNING )
					{
						#if ( _NORMALMAP )
						{
							MatrixPaletteSkinning( i.vPositionOs.xyzw, i.vNormalOs.xyz, i.vTangentUOs_flTangentVSign.xyz, i.vBoneIndices.xyzw );
						}
						#else
						{
							MatrixPaletteSkinning( i.vPositionOs.xyzw, i.vNormalOs.xyz, i.vBoneIndices.xyzw );
						}
						#endif
					}
					#endif

					// Position
					i.vPositionOs.xyzw *= g_flValveGlobalVertexScale; // Used to "hide" all valve materials for debugging
					float3 vPositionWs = mul( unity_ObjectToWorld, i.vPositionOs.xyzw ).xyz;
					#if ( !S_UNLIT )
					{
						o.vPositionWs.xyz = vPositionWs.xyz;
					}
					#endif
					o.vPositionPs.xyzw = mul( UNITY_MATRIX_MVP, i.vPositionOs.xyzw );

					// Normal
					float3 vNormalWs = UnityObjectToWorldNormal( i.vNormalOs.xyz );
					#if ( !S_UNLIT )
					{
						o.vNormalWs.xyz = vNormalWs.xyz;
					}
					#endif

					#if ( _NORMALMAP )
					{
						// TangentU and TangentV
						float3 vTangentUWs = UnityObjectToWorldDir( i.vTangentUOs_flTangentVSign.xyz ); // Transform tangentU into world space
						//vTangentUWs.xyz = normalize( vTangentUWs.xyz - ( vNormalWs.xyz * dot( vTangentUWs.xyz, vNormalWs.xyz ) ) ); // Force tangentU perpendicular to normal and normalize

						o.vTangentUWs.xyz = vTangentUWs.xyz;
						o.vTangentVWs.xyz = cross( vNormalWs.xyz, vTangentUWs.xyz ) * i.vTangentUOs_flTangentVSign.w;
					}
					#endif

					#if ( S_WORLD_ALIGNED_TEXTURE )
					{
						float3 vTexturePositionScaledWs = ( vPositionWs.xyz - g_vWorldAlignedTexturePosition.xyz ) / g_vWorldAlignedTextureSize.xyz;
						o.vTextureCoords.x = dot( vTexturePositionScaledWs.xyz, g_vWorldAlignedNormalTangentU.xyz );
						o.vTextureCoords.y = dot( vTexturePositionScaledWs.xyz, g_vWorldAlignedNormalTangentV.xyz );
						#if ( _DETAIL )
						{
							o.vTextureCoords.zw = TRANSFORM_TEX( o.vTextureCoords.xy, _DetailAlbedoMap );
						}
						#endif
					}
					#else
					{
						// Texture coords (Copied from Unity's TexCoords() helper function)
						o.vTextureCoords.xy = TRANSFORM_TEX( i.vTexCoord0, _MainTex );
						#if ( _DETAIL )
						{
							o.vTextureCoords.zw = TRANSFORM_TEX( ( ( _UVSec == 0 ) ? i.vTexCoord0 : i.vTexCoord1 ), _DetailAlbedoMap );
						}
						#endif
					}
					#endif

					// Indirect lighting uv's or light probe
					#if ( S_OVERRIDE_LIGHTMAP )
					{
						o.vLightmapUV.xy = i.vTexCoord1.xy;
					}
					#elif ( LIGHTMAP_ON )
					{
						// Static lightmaps
						o.vLightmapUV.xy = i.vTexCoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
					}
					#endif

					#if ( DYNAMICLIGHTMAP_ON )
					{
						o.vLightmapUV.zw = i.vTexCoord2.xy * unity_DynamicLightmapST.xy + unity_DynamicLightmapST.zw;
					}
					#endif

					#if ( D_VALVE_FOG )
					{
						o.vFogCoords.xy = CalculateFogCoords( vPositionWs.xyz );
					}
					#endif

					return o;
				}

				// MainPs ---------------------------------------------------------------------------------------------------------------------------------------------------
				#define g_vColorTint _Color
				#define g_tColor _MainTex
				#define g_tNormalMap _BumpMap
				#define g_flBumpScale _BumpScale
				#define g_vReflectance _SpecColor
				#define g_tReflectanceGloss _SpecGlossMap
				#define g_flGlossScale _Glossiness
				#define g_tDetailAlbedo _DetailAlbedoMap
				#define g_tDetailNormal _DetailNormalMap
				#define g_flDetailNormalScale _DetailNormalMapScale

				float g_flReflectanceScale = 1.0;
				float g_flReflectanceBias = 0.0;

				float _OcclusionStrengthDirectDiffuse = 1.0;
				float _OcclusionStrengthDirectSpecular = 1.0;
				float _OcclusionStrengthIndirectDiffuse = 1.0;
				float _OcclusionStrengthIndirectSpecular = 1.0;

				float _FogMultiplier = 1.0;

				struct CommonData {
					
					float4 vAlbedoTexel;
					float3 vAlbedo;
					
					float3 vTangentUWs;
					float3 vTangentVWs;

					float3 vGeometricNormalWs;
					float3 vNormalWs;
					float3 vNormalTs;

				};

				struct PS_OUTPUT
				{
					float4 vColor : SV_Target0;
				};


				//-------------------------------------------------------------------------------------------------------------------------------------------------------------
				// BaseDiffuse
				//-------------------------------------------------------------------------------------------------------------------------------------------------------------
				float4 BaseDiffuse( PS_INPUT i, CommonData cd
					#if ( S_RENDER_BACKFACES )
						, bool bIsFrontFace : SV_IsFrontFace
					#endif
					)
				{
					float4 o = float4(0.0, 0.0, 0.0, 0.0);

					//-----------//
					// Roughness //
					//-----------//
					float2 vRoughness = float2( 1.0, 1.0 );// vNormalTexel.rb;

					float3 basevAlbedo = cd.vAlbedo;
					/*
					{
						float2 vMetallicGloss;// = MetallicGloss( i.vTextureCoords.xy );
						#ifdef _METALLICGLOSSMAP
							vMetallicGloss.xy = tex2D(_MetallicGlossMap, i.vTextureCoords.xy).ra;
						#else
							vMetallicGloss.xy = half2(_Metallic, _Glossiness);
						#endif

						float flOneMinusReflectivity;
						float3 vSpecColor;
						float3 diffColor = DiffuseAndSpecularFromMetallic( basevAlbedo.rgb, vMetallicGloss.x, vSpecColor, flOneMinusReflectivity);
						basevAlbedo.rgb = diffColor.rgb;
					}
					*/

					/*
					float3 vReflectance = float3( 0.0, 0.0, 0.0 );
					float flGloss = 0.0;

						float4 vReflectanceGloss; // = SpecularGloss( i.vTextureCoords.xy );
						#ifdef _SPECGLOSSMAP
							vReflectanceGloss.rgba = tex2D(_SpecGlossMap, i.vTextureCoords.xy);
						#else
							vReflectanceGloss.rgba = float4(_SpecColor.rgb, _Glossiness);
						#endif

						vReflectanceGloss.rgb = ( vReflectanceGloss.rgb * g_flReflectanceScale.xxx ) + g_flReflectanceBias.xxx;
						vReflectance.rgb = vReflectanceGloss.rgb;
						flGloss = vReflectanceGloss.a;
						*/

					//----------//
					// Lighting //
					//----------//
					LightingTerms_t lightingTerms;
					lightingTerms.vDiffuse.rgb = float3( 1.0, 1.0, 1.0 );
					lightingTerms.vSpecular.rgb = float3( 0.0, 0.0, 0.0 );
					lightingTerms.vIndirectDiffuse.rgb = float3( 0.0, 0.0, 0.0 );
					lightingTerms.vIndirectSpecular.rgb = float3( 0.0, 0.0, 0.0 );
					lightingTerms.vTransmissiveSunlight.rgb = float3( 0.0, 0.0, 0.0 );

					float flFresnelExponent = 5.0;
					float flMetalness = 0.0f;

					#if ( !S_UNLIT )
					{
						float4 vLightmapUV = float4( 0.0, 0.0, 0.0, 0.0 );
						#if ( S_OVERRIDE_LIGHTMAP || LIGHTMAP_ON || DYNAMICLIGHTMAP_ON )
						{
							vLightmapUV.xy = i.vLightmapUV.xy;
							#if ( DYNAMICLIGHTMAP_ON )
							{
								vLightmapUV.zw = i.vLightmapUV.zw;
							}
							#endif
						}
						#endif

						// Compute lighting
						//lightingTerms = ComputeLighting( i.vPositionWs.xyz, cd.vNormalWs.xyz, cd.vTangentUWs.xyz, cd.vTangentVWs.xyz, vRoughness.xy, vReflectance.rgb, flFresnelExponent, vLightmapUV.xyzw );
						lightingTerms = ComputeLightingDiffuseOnly( i.vPositionWs.xyz, cd.vNormalWs.xyz, cd.vTangentUWs.xyz, cd.vTangentVWs.xyz, vRoughness.xy, vLightmapUV.xyzw );

						#if ( S_OCCLUSION )
						{
							float flOcclusion = tex2D( _OcclusionMap, i.vTextureCoords.xy ).g;
							lightingTerms.vDiffuse.rgb *= LerpOneTo( flOcclusion, _OcclusionStrength * _OcclusionStrengthDirectDiffuse );
							lightingTerms.vSpecular.rgb *= LerpOneTo( flOcclusion, _OcclusionStrength * _OcclusionStrengthDirectSpecular );
							lightingTerms.vIndirectDiffuse.rgb *= LerpOneTo( flOcclusion, _OcclusionStrength * _OcclusionStrengthIndirectDiffuse );
							lightingTerms.vIndirectSpecular.rgb *= LerpOneTo( flOcclusion, _OcclusionStrength * _OcclusionStrengthIndirectSpecular );
						}
						#endif
					}
					#endif

					// Diffuse
					o.rgb = ( lightingTerms.vDiffuse.rgb + lightingTerms.vIndirectDiffuse.rgb ) * basevAlbedo.rgb;

					/*
					// Specular
					#if ( !S_SPECULAR_NONE )
					{
						o.rgb += lightingTerms.vSpecular.rgb;
					}
					#endif
					o.rgb += lightingTerms.vIndirectSpecular.rgb; // Indirect specular applies its own fresnel in the forward lighting header file
					*/

					// Emission - Unity just adds the emissive term at the end instead of adding it to the diffuse lighting term. Artists may want both options.
					float3 vEmission = Emission( i.vTextureCoords.xy );
					o.rgb += vEmission.rgb;

					return o;
				}


				//-------------------------------------------------------------------------------------------------------------------------------------------------------------
				// Flakes
				//-------------------------------------------------------------------------------------------------------------------------------------------------------------
				float4 Flakes( PS_INPUT i, CommonData cd
					#if ( S_RENDER_BACKFACES )
						, bool bIsFrontFace : SV_IsFrontFace
					#endif
					)
				{
					float4 o = float4(0.0, 0.0, 0.0, 0.0);

					float3 flakedvNormalTs = cd.vNormalTs;
					float3 flakedvNormalWs = cd.vNormalWs;
					#if (E_USE_FLAKES)
					{
						// flakes
						//vNormalTs.xyz = voronoi(i.vTextureCoords.xy * 2000.0);
						//flakedvNormalTs.xyz = randNormal(i.vTextureCoords.xy);
						flakedvNormalTs.xyz = randNormal(floor(i.vTextureCoords.xy * 3000.0)); // scale with view distance...
						//flakedvNormalTs.xyz = bar(floor(i.vPositionWs.xyz * 10000.0));
						//vNormalTs.y = -vNormalTs.y;

						// less flakes at an angle
						float vdotn = dot(cd.vGeometricNormalWs.xyz, normalize(_WorldSpaceCameraPos - i.vPositionWs.xyz));

						//vdotn = vdotn * vdotn * vdotn;

						//o.vColor = float4(vdotn,vdotn,vdotn,1);
						//return o;

						flakedvNormalTs.xyz = normalize(lerp(float3(0,0,1), flakedvNormalTs.xyz, vdotn));
						
						// Convert to world space
						flakedvNormalWs.xyz = Vec3TsToWsNormalized( flakedvNormalTs.xyz, cd.vGeometricNormalWs.xyz, cd.vTangentUWs.xyz, cd.vTangentVWs.xyz );

					}
					#endif

					//-----------//
					// Roughness //
					//-----------//
					float2 vRoughness = float2( 0.6, 0.6 );// vNormalTexel.rb;
					//#if ( S_HIGH_QUALITY_GLOSS )
					//{
					//	float4 vGlossTexel = Tex2D( g_tGloss, i.vTextureCoords.xy );
					//	vRoughness.xy += vGlossTexel.ag;
					//}
					//#endif

					// Reflectance and gloss
					float3 vReflectance = float3( 0.0, 0.0, 0.0 );
					float flGloss = 0.0;
					#if ( S_SPECULAR_METALLIC )
					{
						float2 vMetallicGloss;// = MetallicGloss( i.vTextureCoords.xy );
						#ifdef _METALLICGLOSSMAP
							vMetallicGloss.xy = tex2D(_MetallicGlossMap, i.vTextureCoords.xy).ra;
						#else
							vMetallicGloss.xy = half2(_Metallic, _Glossiness);
						#endif

						float flOneMinusReflectivity;
						float3 vSpecColor;
						float3 diffColor = DiffuseAndSpecularFromMetallic( cd.vAlbedo.rgb, vMetallicGloss.x, /*out*/ vSpecColor, /*out*/ flOneMinusReflectivity);
						//vAlbedo.rgb = diffColor.rgb;

						vReflectance.rgb = vSpecColor.rgb;
						flGloss = vMetallicGloss.y;
					}
					#elif ( S_SPECULAR_BLINNPHONG )
					{
						float4 vReflectanceGloss; // = SpecularGloss( i.vTextureCoords.xy );
						#ifdef _SPECGLOSSMAP
							vReflectanceGloss.rgba = tex2D(_SpecGlossMap, i.vTextureCoords.xy);
						#else
							vReflectanceGloss.rgba = float4(_SpecColor.rgb, _Glossiness);
						#endif

						vReflectanceGloss.rgb = ( vReflectanceGloss.rgb * g_flReflectanceScale.xxx ) + g_flReflectanceBias.xxx;
						vReflectance.rgb = vReflectanceGloss.rgb;
						flGloss = vReflectanceGloss.a;
					}
					#endif

					vRoughness.xy = ( 1.0 - flGloss ).xx;
					#if ( !S_SPECULAR_NONE )
					{
						vRoughness.xy = AdjustRoughnessByGeometricNormal( vRoughness.xy, cd.vGeometricNormalWs.xyz );
					}
					#endif

					//-------------------//
					// Specular Lighting //
					//-------------------//
					LightingTerms_t lightingTerms;
					lightingTerms.vDiffuse.rgb = float3( 1.0, 1.0, 1.0 );
					lightingTerms.vSpecular.rgb = float3( 0.0, 0.0, 0.0 );
					lightingTerms.vIndirectDiffuse.rgb = float3( 0.0, 0.0, 0.0 );
					lightingTerms.vIndirectSpecular.rgb = float3( 0.0, 0.0, 0.0 );
					lightingTerms.vTransmissiveSunlight.rgb = float3( 0.0, 0.0, 0.0 );

					float flFresnelExponent = 5.0;
					float flMetalness = 0.0f;

					#if ( !S_UNLIT )
					{
						float4 vLightmapUV = float4( 0.0, 0.0, 0.0, 0.0 );
						#if ( S_OVERRIDE_LIGHTMAP || LIGHTMAP_ON || DYNAMICLIGHTMAP_ON )
						{
							vLightmapUV.xy = i.vLightmapUV.xy;
							#if ( DYNAMICLIGHTMAP_ON )
							{
								vLightmapUV.zw = i.vLightmapUV.zw;
							}
							#endif
						}
						#endif

						// Compute lighting
						lightingTerms = ComputeLighting( i.vPositionWs.xyz, flakedvNormalWs.xyz, cd.vTangentUWs.xyz, cd.vTangentVWs.xyz, vRoughness.xy, vReflectance.rgb, flFresnelExponent, vLightmapUV.xyzw );

						#if ( S_OCCLUSION )
						{
							float flOcclusion = tex2D( _OcclusionMap, i.vTextureCoords.xy ).g;
							lightingTerms.vDiffuse.rgb *= LerpOneTo( flOcclusion, _OcclusionStrength * _OcclusionStrengthDirectDiffuse );
							lightingTerms.vSpecular.rgb *= LerpOneTo( flOcclusion, _OcclusionStrength * _OcclusionStrengthDirectSpecular );
							lightingTerms.vIndirectDiffuse.rgb *= LerpOneTo( flOcclusion, _OcclusionStrength * _OcclusionStrengthIndirectDiffuse );
							lightingTerms.vIndirectSpecular.rgb *= LerpOneTo( flOcclusion, _OcclusionStrength * _OcclusionStrengthIndirectSpecular );
						}
						#endif
					}
					#endif

					// Diffuse
					//o.rgb = ( lightingTerms.vDiffuse.rgb + lightingTerms.vIndirectDiffuse.rgb ) * basevAlbedo.rgb;

					// Specular
					#if ( !S_SPECULAR_NONE )
					{
						o.rgb += lightingTerms.vSpecular.rgb;
					}
					#endif
					//o.rgb += lightingTerms.vIndirectSpecular.rgb; // Indirect specular applies its own fresnel in the forward lighting header file

					return o;
				}

				//-------------------------------------------------------------------------------------------------------------------------------------------------------------
				// Clearoat
				//-------------------------------------------------------------------------------------------------------------------------------------------------------------
				float4 Clearcoat( PS_INPUT i, CommonData cd
					#if ( S_RENDER_BACKFACES )
						, bool bIsFrontFace : SV_IsFrontFace
					#endif
					)
				{
					float4 o = float4(0.0, 0.0, 0.0, 0.0);

					//-------------//
					// Orange Peel //
					//-------------//

					//-----------//
					// Roughness //
					//-----------//
					float2 vRoughness = float2( 0.6, 0.6 );// vNormalTexel.rb;
					//#if ( S_HIGH_QUALITY_GLOSS )
					//{
					//	float4 vGlossTexel = Tex2D( g_tGloss, i.vTextureCoords.xy );
					//	vRoughness.xy += vGlossTexel.ag;
					//}
					//#endif

					// Reflectance and gloss
					float3 vReflectance = float3( 0.0, 0.0, 0.0 );
					float flGloss = 0.0;
					{
						float4 vReflectanceGloss; // = SpecularGloss( i.vTextureCoords.xy );
						#ifdef _SPECGLOSSMAP
							vReflectanceGloss.rgba = tex2D(_SpecGlossMap, i.vTextureCoords.xy);
						#else
							vReflectanceGloss.rgba = float4(_SpecColor.rgb, 0.95);
						#endif

						vReflectanceGloss.rgb = ( vReflectanceGloss.rgb * g_flReflectanceScale.xxx ) + g_flReflectanceBias.xxx;
						vReflectance.rgb = vReflectanceGloss.rgb;
						flGloss = vReflectanceGloss.a;
					}

					vRoughness.xy = ( 1.0 - flGloss ).xx;
					#if ( !S_SPECULAR_NONE )
					{
						vRoughness.xy = AdjustRoughnessByGeometricNormal( vRoughness.xy, cd.vGeometricNormalWs.xyz );
					}
					#endif

					//--------------------//
					// Clearcoat Lighting //
					//--------------------//
					LightingTerms_t lightingTerms;
					lightingTerms.vDiffuse.rgb = float3( 1.0, 1.0, 1.0 );
					lightingTerms.vSpecular.rgb = float3( 0.0, 0.0, 0.0 );
					lightingTerms.vIndirectDiffuse.rgb = float3( 0.0, 0.0, 0.0 );
					lightingTerms.vIndirectSpecular.rgb = float3( 0.0, 0.0, 0.0 );
					lightingTerms.vTransmissiveSunlight.rgb = float3( 0.0, 0.0, 0.0 );

					float flFresnelExponent = 5.0;
					float flMetalness = 0.0f;

					#if ( !S_UNLIT )
					{
						float4 vLightmapUV = float4( 0.0, 0.0, 0.0, 0.0 );
						#if ( S_OVERRIDE_LIGHTMAP || LIGHTMAP_ON || DYNAMICLIGHTMAP_ON )
						{
							vLightmapUV.xy = i.vLightmapUV.xy;
							#if ( DYNAMICLIGHTMAP_ON )
							{
								vLightmapUV.zw = i.vLightmapUV.zw;
							}
							#endif
						}
						#endif

						// Compute lighting
						lightingTerms = ComputeLighting( i.vPositionWs.xyz, cd.vNormalWs.xyz, cd.vTangentUWs.xyz, cd.vTangentVWs.xyz, vRoughness.xy, vReflectance.rgb, flFresnelExponent, vLightmapUV.xyzw );

						#if ( S_OCCLUSION )
						{
							float flOcclusion = tex2D( _OcclusionMap, i.vTextureCoords.xy ).g;
							lightingTerms.vDiffuse.rgb *= LerpOneTo( flOcclusion, _OcclusionStrength * _OcclusionStrengthDirectDiffuse );
							lightingTerms.vSpecular.rgb *= LerpOneTo( flOcclusion, _OcclusionStrength * _OcclusionStrengthDirectSpecular );
							lightingTerms.vIndirectDiffuse.rgb *= LerpOneTo( flOcclusion, _OcclusionStrength * _OcclusionStrengthIndirectDiffuse );
							lightingTerms.vIndirectSpecular.rgb *= LerpOneTo( flOcclusion, _OcclusionStrength * _OcclusionStrengthIndirectSpecular );
						}
						#endif
					}
					#endif

					// Specular
					#if ( !S_SPECULAR_NONE )
					{
						o.rgb += lightingTerms.vSpecular.rgb;
					}
					#endif
					o.rgb += lightingTerms.vIndirectSpecular.rgb; // Indirect specular applies its own fresnel in the forward lighting header file

					return o;
				}


				//-------------------------------------------------------------------------------------------------------------------------------------------------------------
				// Main
				//-------------------------------------------------------------------------------------------------------------------------------------------------------------
				PS_OUTPUT MainPs( PS_INPUT i
					#if ( S_RENDER_BACKFACES )
						, bool bIsFrontFace : SV_IsFrontFace
					#endif
					)
				{
					PS_OUTPUT o = (PS_OUTPUT)0;

					//-----------------------------------------------------------//
					// Negate the world normal if we are rendering the back face //
					//-----------------------------------------------------------//
					#if ( S_RENDER_BACKFACES )
					{
						i.vNormalWs.xyz *= ( bIsFrontFace ? 1.0 : -1.0 );
					}
					#endif

					CommonData cd = (CommonData)0;

					cd.vAlbedoTexel = tex2D( g_tColor, i.vTextureCoords.xy ) * g_vColorTint.rgba;
					cd.vAlbedo = cd.vAlbedoTexel.rgb;

					// Apply detail to albedo
					#if ( _DETAIL )
					{
						float flDetailMask = DetailMask( i.vTextureCoords.xy );
						float3 vDetailAlbedo = tex2D( g_tDetailAlbedo, i.vTextureCoords.zw ).rgb;
						#if ( _DETAIL_MULX2 )
							cd.vAlbedo.rgb *= LerpWhiteTo( vDetailAlbedo.rgb * unity_ColorSpaceDouble.rgb, flDetailMask );
						#elif ( _DETAIL_MUL )
							cd.vAlbedo.rgb *= LerpWhiteTo( vDetailAlbedo.rgb, flDetailMask );
						#elif ( _DETAIL_ADD )
							cd.vAlbedo.rgb += vDetailAlbedo.rgb * flDetailMask;
						#elif ( _DETAIL_LERP )
							cd.vAlbedo.rgb = lerp( vAlbedo.rgb, vDetailAlbedo.rgb, flDetailMask );
						#endif
					}
					#endif

					//--------------//
					// Translucency //
					//--------------//
					#if ( _ALPHATEST_ON )
					{
						clip( cd.vAlbedoTexel.a - _Cutoff );
					}
					#endif

					#if ( _ALPHAPREMULTIPLY_ON )
					{
						cd.vAlbedo.rgb *= cd.vAlbedoTexel.a;
					}
					#endif

					#if ( _ALPHABLEND_ON || _ALPHAPREMULTIPLY_ON )
					{
						o.vColor.a = cd.vAlbedoTexel.a;
					}
					#else
					{
						o.vColor.a = 1.0;
					}
					#endif

					//---------------//
					// Tangent Space //
					//---------------//
					cd.vTangentUWs = float3( 1.0, 0.0, 0.0 );
					cd.vTangentVWs = float3( 0.0, 1.0, 0.0 );
					#if ( _NORMALMAP )
					{
						cd.vTangentUWs.xyz = i.vTangentUWs.xyz;
						cd.vTangentVWs.xyz = i.vTangentVWs.xyz;
					}
					#endif

					//--------//
					// Normal //
					//--------//
					cd.vGeometricNormalWs = float3( 0.0, 0.0, 1.0 );
					#if ( !S_UNLIT )
					{
						i.vNormalWs.xyz = normalize( i.vNormalWs.xyz );
						cd.vGeometricNormalWs.xyz = i.vNormalWs.xyz;
					}
					#endif

					cd.vNormalWs = cd.vGeometricNormalWs.xyz;
					cd.vNormalTs = float3( 0.0, 0.0, 1.0 );
					#if ( _NORMALMAP )
					{
						cd.vNormalTs.xyz = UnpackScaleNormal( tex2D( g_tNormalMap, i.vTextureCoords.xy ), g_flBumpScale );
						//vNormalTs.y = -vNormalTs.y;

						// Apply detail to tangent normal
						#if ( _DETAIL )
						{
							float flDetailMask = DetailMask( i.vTextureCoords.xy );
							float3 vDetailNormalTs = UnpackScaleNormal( tex2D( g_tDetailNormal, i.vTextureCoords.zw ), g_flDetailNormalScale );
							#if ( _DETAIL_LERP )
							{
								cd.vNormalTs.xyz = lerp( cd.vNormalTs.xyz, vDetailNormalTs.xyz, flDetailMask );
							}
							#else				
							{
								cd.vNormalTs.xyz = lerp( cd.vNormalTs.xyz, BlendNormals( cd.vNormalTs.xyz, vDetailNormalTs.xyz ), flDetailMask );
							}
							#endif
						}
						#endif

						// Convert to world space
						cd.vNormalWs.xyz = Vec3TsToWsNormalized( cd.vNormalTs.xyz, cd.vGeometricNormalWs.xyz, cd.vTangentUWs.xyz, cd.vTangentVWs.xyz );
					}
					#endif

					#if ( S_RENDER_BACKFACES )
					o.vColor = BaseDiffuse(i, cd, bIsFrontFace) + Flakes(i, cd, bIsFrontFace) + Clearcoat(i, cd, bIsFrontFace);
					#else
					o.vColor = BaseDiffuse(i, cd) + Flakes(i, cd) + Clearcoat(i, cd);
					#endif
					
					// Fog
					#if ( D_VALVE_FOG )
					{
						o.vColor.rgb = ApplyFog( o.vColor.rgb, i.vFogCoords.xy, _FogMultiplier );
					}
					#endif

					// Dither to fix banding artifacts
					o.vColor.rgb += ScreenSpaceDither( i.vPositionPs.xy );
					
					return o;
				}

			ENDCG
		}

		//-------------------------------------------------------------------------------------------------------------------------------------------------------------
		// Shadow rendering pass
		//-------------------------------------------------------------------------------------------------------------------------------------------------------------
		//Pass
		//{
		//	Name "ShadowCaster"
		//	Tags { "LightMode" = "ShadowCaster" }
		//	
		//	ZWrite On ZTest LEqual
		//
		//	CGPROGRAM
		//		#pragma target 5.0
		//		// TEMPORARY: GLES2.0 temporarily disabled to prevent errors spam on devices without textureCubeLodEXT
		//		#pragma exclude_renderers gles
		//		
		//		// -------------------------------------
		//		#pragma shader_feature _ _ALPHATEST_ON _ALPHABLEND_ON _ALPHAPREMULTIPLY_ON
		//		#pragma multi_compile_shadowcaster
		//
		//		#pragma vertex vertShadowCaster
		//		#pragma fragment fragShadowCaster
		//
		//		#include "UnityStandardShadow.cginc"
		//	ENDCG
		//}

		//-------------------------------------------------------------------------------------------------------------------------------------------------------------
		// Extracts information for lightmapping, GI (emission, albedo, ...)
		// This pass it not used during regular rendering.
		//-------------------------------------------------------------------------------------------------------------------------------------------------------------
		Pass
		{
			Name "META" 
			Tags { "LightMode"="Meta" }
		
			Cull Off
		
			CGPROGRAM
				#pragma only_renderers d3d11

				#pragma vertex vert_meta
				#pragma fragment frag_meta
		
				#pragma shader_feature _EMISSION
				#pragma shader_feature _METALLICGLOSSMAP
				#pragma shader_feature ___ _DETAIL_MULX2
		
				#include "UnityStandardMeta.cginc"
			ENDCG
		}
	}

	Fallback "Valve/vr_carpaint_mac"
	CustomEditor "ValveCarShaderGUI"
}
