﻿Shader "Unlit/vr_heatmap"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_HotColor("HotColor", Color) = (1,0,0,1)
		_ColdColor("ColdColor", Color) = (0,0,1,1)
	}
	SubShader
	{
		Tags{ "RenderType" = "Opaque" }
		LOD 200

		Pass
		{
			Tags{ "LightMode" = "Always" }

			Fog{ Mode Off }
			ZWrite On
			ZTest LEqual
			Cull Back
			Lighting Off

			CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma fragmentoption ARB_precision_hint_fastest

			fixed4 _Color;
			uniform sampler3D _HeatMapIndex;
			uniform sampler3D _HeatMapVoxels;
			uniform float4 _HeatMapOffset;
			uniform float4 _HeatMapSize;
			uniform float4 _VoxelScale;
			uniform float _VoxelSize;
			float4 _HotColor;
			float4 _ColdColor;

			struct appdata
			{
				float4 vertex : POSITION;
			};

			struct v2f
			{
				float4 vertex : POSITION;
				float3 uvz : TEXCOORD0;
			};

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uvz = (v.vertex.xyz - _HeatMapOffset.xyz);
				return o;
			}

			float4 getVoxel(float3 pos)
			{
				float3 index = tex3D(_HeatMapIndex, pos / _HeatMapSize.xyz).xyz;
				index = (round(index * 255) + frac(pos)) / _VoxelScale.xyz; // Magic to fix byte to float precision errors.
				return tex3D(_HeatMapVoxels, index);
			}

			fixed4 frag(v2f i) : COLOR
			{
				float3 y0 = i.uvz;
				float3 y1 = i.uvz + float3(0, _VoxelSize, 0);
				float3 x1 = float3(_VoxelSize, 0, 0);
				float4 x0y0z0 = getVoxel(y0);
				float4 x1y0z0 = getVoxel(y0 + x1);
				float4 x0y1z0 = getVoxel(y1);
				float4 x1y1z0 = getVoxel(y1 + x1);

				y0.z += _VoxelSize;
				y1.z += _VoxelSize;
				float4 x0y0z1 = getVoxel(y0);
				float4 x1y0z1 = getVoxel(y0 + x1);
				float4 x0y1z1 = getVoxel(y1);
				float4 x1y1z1 = getVoxel(y1 + x1);

				float3 f = frac(i.uvz / _VoxelSize);
				float4 y0z0 = lerp(x0y0z0, x1y0z0, f.x);
				float4 y1z0 = lerp(x0y1z0, x1y1z0, f.x);
				float4 y0z1 = lerp(x0y0z1, x1y0z1, f.x);
				float4 y1z1 = lerp(x0y1z1, x1y1z1, f.x);

				float4 z0 = lerp(y0z0, y1z0, f.y);
				float4 z1 = lerp(y0z1, y1z1, f.y);

				float4 voxel = lerp(z0, z1, f.z);

				float4 col = lerp(_ColdColor, _HotColor, voxel);
				return fixed4(col) *_Color;
			}
			ENDCG
		}
	}
}
