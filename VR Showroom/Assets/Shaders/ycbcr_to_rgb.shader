﻿Shader "Hidden/ycbcr_to_rgb"
{
	Properties
	{
		_yTex ("Texture", 2D) = "white" {}
		_cbTex ("Texture", 2D) = "white" {}
		_crTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.uv;
				o.uv.y = 1 - o.uv.y;
				return o;
			}

			sampler2D _yTex;
			sampler2D _cbTex;
			sampler2D _crTex;

			float4 frag (v2f i) : SV_Target
			{
				float y = tex2D(_yTex, i.uv).a;
				float cb = tex2D(_cbTex, i.uv).a;
				float cr = tex2D(_crTex, i.uv).a;

				float4 col;

				col.r = y + 1.4075 * (cr - 0.5);
                col.g = y - 0.3455 * (cb - 0.5) - (0.7169 * (cr - 0.5));
                col.b = y + 1.7790 * (cb - 0.5);
                col.a = 1.0;

                col.rgb = pow(col.rgb ,2.2f);

				return col;
			}

			ENDCG
		}
	}
}
