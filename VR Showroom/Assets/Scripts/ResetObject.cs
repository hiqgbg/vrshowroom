﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ResetObject : MonoBehaviour {

    private Vector3 m_originalPosition;
    private Quaternion m_orignalRotation;

    private static List<ResetObject> ResetObjects = new List<ResetObject>();

    public void ResetAllPositions()
    {
        foreach (ResetObject resetObject in ResetObjects)
        {
            resetObject.ResetPosition();
        }
    }

    // Use this for initialization
    void Start () {
        m_originalPosition = transform.position;
        m_orignalRotation = transform.rotation;

        ResetObjects.Add(this);
	}

    void ResetPosition()
    {
        Rigidbody rb = GetComponent<Rigidbody>();
        transform.position = m_originalPosition;
        transform.rotation = m_orignalRotation;

		if (rb)
        	rb.Sleep();
    }
}
