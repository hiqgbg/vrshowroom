﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;


[RequireComponent (typeof (Renderer))]
[RequireComponent (typeof (AudioSource))]
[RequireComponent (typeof (HiveMoviePlayer))]
public class MoviePlayer : MonoBehaviour
{
	public List<AudioClip> audioClips = new List<AudioClip>();
	public List<string> moviePaths = new List<string>();

	public string readFromFolder = "";

	public bool autostart = false;
	public bool continuousPlay = false;

	private int currentlyPlaying = 0;
	private HiveMoviePlayer player;
	private Coroutine continuousPlayCoroutine;

    public void Start()
    {
		player = GetComponent<HiveMoviePlayer> (); 

		if (readFromFolder != "")
		{
			findMovies ();
		}

		SetCurrentMovie (0);

		if (autostart)
		{
			play();
		}
    }

	private void SetCurrentMovie(int index)
	{
		if (index >= moviePaths.Count) {
			Debug.LogWarning ("SetCurrentMovie index (" + index + ") out of range [0," + moviePaths.Count + "]");
			return;
		}

		currentlyPlaying = index;

		string fileName = Path.GetFileNameWithoutExtension (moviePaths [currentlyPlaying]);
		string[] splitAssetPath = readFromFolder.Split(new[] { "Resources/" }, System.StringSplitOptions.None);
		AudioClip movieAudio = Resources.Load<AudioClip>(splitAssetPath[1] + fileName + "Audio");

		player.SetMovie (moviePaths [currentlyPlaying], movieAudio);
	}

	public void play()
    {
		if (continuousPlay)
		{
			if (continuousPlayCoroutine != null) 
			{
				StopCoroutine (continuousPlayCoroutine);
			}
			continuousPlayCoroutine = StartCoroutine (ContinuousPlay());
		}

		player.Play();
    }

	public void pause()
	{
		if (!player.IsDone() && !player.IsPaused())
		{
			player.Pause();
		} 
		else
		{
			player.Continue ();
		}
	}

    public void forward()
    {
		//player.Stop();

		if (currentlyPlaying + 1 >= moviePaths.Count) 
		{
			SetCurrentMovie (0);
		}
        else
        {
			SetCurrentMovie(currentlyPlaying + 1);
        }
    }

    public void rewind()
    {
		//player.Stop();

		if (currentlyPlaying - 1 < 0) 
		{
			SetCurrentMovie(moviePaths.Count - 1);
		}
        else
        {
			SetCurrentMovie(currentlyPlaying - 1);
        }
    }

	public void stop()
	{
		if (continuousPlayCoroutine != null) 
		{
			StopCoroutine (continuousPlayCoroutine);
			continuousPlayCoroutine = null;
		}
		player.Stop();
	}

	void findMovies()
	{
		string path = Application.dataPath;
		IEnumerable<string> files = Directory.GetFiles(path + "/" + readFromFolder, "*.*", SearchOption.AllDirectories);

		foreach (string file in files)
		{
			string[] allParts = file.Split(Path.DirectorySeparatorChar);
			string lastPart = allParts[allParts.Length - 1];

			if ((lastPart.Contains(".ogv") || lastPart.Contains(".ogg")) && !lastPart.Contains(".meta"))
			{
				moviePaths.Add (file);
			}
		}
	}

	IEnumerator ContinuousPlay()
    {
		while (true) {

			float videoLength = player.Length ();
			float timeUntilMovieEnd = videoLength - player.CurrentPosition();
			if (videoLength == Mathf.Infinity)
				timeUntilMovieEnd = 1.0f;
			
			yield return new WaitForSeconds (timeUntilMovieEnd + 0.1f);

			if (!continuousPlay)
				break;
			
			if (player.IsDone ())
			{
				forward ();
				player.Play ();
			}
		}
    }
	
	void Update()
	{
		if (Input.GetKeyDown (KeyCode.LeftArrow))
			stop ();

		if (Input.GetKeyDown (KeyCode.RightArrow))
		{
			forward ();
			play ();
		}
	}
}