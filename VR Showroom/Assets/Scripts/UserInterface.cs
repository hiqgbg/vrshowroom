﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UserInterface : MonoBehaviour {
	public GameObject controllerTooltip;

	public float fadeTime = 1.0f;
	public int showTime = 15;

	private VRTK.VRTK_ControllerEvents events;

	private List<Color> solidColors;
	private List<Color> fadedColors;

	private VRTK.VRTK_ControllerTooltips toolTips;

	private bool fading = false;
	private bool faded = false;

	private int startTime = 0;

	// Use this for initialization
	void Start () {

		if (events == null)
		{
			events = GetComponent<VRTK.VRTK_ControllerEvents>();
		}
		if (events == null)
		{
			Debug.LogError ("The radial menu must be a child of the controller or be set in the inspector!");
		} 
		else
		{
			events.TouchpadPressed += new VRTK.ControllerInteractionEventHandler(DoSomethingPressed);
			events.TriggerPressed += new VRTK.ControllerInteractionEventHandler(DoSomethingPressed);
			events.GripPressed += new VRTK.ControllerInteractionEventHandler(DoSomethingPressed);
			events.ApplicationMenuPressed += new VRTK.ControllerInteractionEventHandler(DoSomethingPressed);
		}

		solidColors = new List<Color>();
		fadedColors = new List<Color>();

		toolTips = controllerTooltip.GetComponent<VRTK.VRTK_ControllerTooltips>();

		Color bg = toolTips.tipBackgroundColor;
		Color text = toolTips.tipTextColor;
		Color line = toolTips.tipLineColor;

		solidColors.Add(bg);
		solidColors.Add(text);
		solidColors.Add(line);

		fadedColors.Add(new Color(bg.r, bg.g, bg.b, 0.0f)); 
		fadedColors.Add(new Color(text.r, text.g, text.b, 0.0f)); 
		fadedColors.Add(new Color(line.r, line.g, line.b, 0.0f));

		ShowTooltip();
	}

	// Update is called once per frame
	void Update () {
		if (!faded && !fading)
		{
			int currentTime = (int)(Time.time + 0.5f);
			if (currentTime > (startTime + showTime))
			{
				StartCoroutine (FadeOut());
			}
		}
	}

	private void DoSomethingPressed(object sender, VRTK.ControllerInteractionEventArgs e)
	{
		if (!faded && !fading)
		{
			StartCoroutine (FadeOut());
		}
	}

	//set fading and lerp from faded to solid over fadeTime
	IEnumerator FadeIn() {
		fading = true; 
		for (float t = 0.0f; t < fadeTime; t += Time.deltaTime)
		{
			toolTips.tipBackgroundColor = Color.Lerp(fadedColors[0], solidColors[0], t/fadeTime);
			toolTips.tipTextColor = Color.Lerp(fadedColors[1], solidColors[1], t/fadeTime);
			toolTips.tipLineColor = Color.Lerp(fadedColors[2], solidColors[2], t/fadeTime);
			yield return null;
		}
		toolTips.tipBackgroundColor = solidColors[0];
		toolTips.tipTextColor = solidColors[1];
		toolTips.tipLineColor = solidColors[2];

		fading = false; 
		faded = false;
	} 
	//set fading and lerp from solid to faded over fadeTime
	IEnumerator FadeOut() { 
		fading = true;
		for (float t = 0.0f; t < fadeTime; t += Time.deltaTime) 
		{ 
			toolTips.tipBackgroundColor = Color.Lerp(solidColors[0], fadedColors[0], t/fadeTime);
			toolTips.tipTextColor = Color.Lerp(solidColors[1], fadedColors[1], t/fadeTime);
			toolTips.tipLineColor = Color.Lerp(solidColors[2], fadedColors[2], t/fadeTime);
			yield return null;
		}
		toolTips.tipBackgroundColor = fadedColors[0];
		toolTips.tipTextColor = fadedColors[1];
		toolTips.tipLineColor = fadedColors[2];

		fading = false;
		faded = true;
	}

	public void ShowTooltip()
	{
		faded = false;
		fading = false;
		startTime = (int) (Time.time + 0.5f);
		StartCoroutine (FadeIn ());
	}
}
