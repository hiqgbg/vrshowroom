﻿using UnityEngine;
using System.Collections;
using VRTK;

public class RemoteControlEventListener : MonoBehaviour {

    public MoviePlayer moviePlayer;
	// Use this for initialization
	void Start () {

        //e.target.transform.parent = this;
        //this.transform.parent = yourParentObject;

        GetComponent<VRTK_InteractableObject>().InteractableObjectTouched += new InteractableObjectEventHandler(DoInteractableTouched);
        GetComponent<VRTK_InteractableObject>().InteractableObjectUntouched += new InteractableObjectEventHandler(DoInteractableUntouched);
        GetComponent<VRTK_InteractableObject>().InteractableObjectUsed += new InteractableObjectEventHandler(DoInteractableUsed);
		GetComponent<VRTK_InteractableObject>().InteractableObjectUnused += new InteractableObjectEventHandler(DoInteractableUnused);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void DebugLogger(string action, GameObject target)
	{
		Debug.Log(action + " by an object named " + target.name);
	}


    void DoInteractableTouched(object sender, InteractableObjectEventArgs e)
    {
        e.interactingObject.gameObject.GetComponent<VRTK_InteractGrab>().hideControllerOnGrab = true;
        e.interactingObject.gameObject.GetComponent<RemoteController>().SetActive(true);
        RemoteController controller = e.interactingObject.gameObject.GetComponent<RemoteController>();
        controller.moviePlayer = moviePlayer;
        controller.playButton = this.transform.FindChild("play");
        controller.pauseButton = this.transform.FindChild("pause");
        controller.forwardButton = this.transform.FindChild("forward");
        controller.rewindButton = this.transform.FindChild("reverse");
    }

    void DoInteractableUntouched(object sender, InteractableObjectEventArgs e)
    {
        //e.interactingObject.gameObject.GetComponent<BezierPointer>().enabled = true;
        //e.interactingObject.gameObject.GetComponentInParent<VRTK_BasicTeleport>().enabled = true;
        e.interactingObject.gameObject.GetComponent<VRTK_InteractGrab>().hideControllerOnGrab = false;
        e.interactingObject.gameObject.GetComponent<RemoteController>().SetActive(false);
    }

    void DoInteractableUsed(object sender, InteractableObjectEventArgs e)
	{
		e.interactingObject.gameObject.GetComponent<BezierPointer> ().enabled = false;
	}

	void DoInteractableUnused(object sender, InteractableObjectEventArgs e)
	{
		e.interactingObject.gameObject.GetComponent<BezierPointer> ().enabled = true;
    }
}
