﻿using UnityEngine;
using System.Collections;

public class Clock : MonoBehaviour {

	public GameObject minutes;
	public GameObject hours;
	public int startTimeOffsetDegrees = 90;

	private int hourAngle = 30;
	private int minuteAngle = 6;

	private int minute = -1;
	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {
		System.DateTime now = System.DateTime.Now;

		if (now.Minute != minute) {
			minute = now.Minute;
			int hour = now.Hour;

			Vector3 localMinuteAngles = minutes.transform.localEulerAngles;
			minutes.transform.localEulerAngles = new Vector3 (localMinuteAngles.x, localMinuteAngles.y, minuteAngle * minute - startTimeOffsetDegrees);

			Vector3 localHourAngles = hours.transform.localEulerAngles;

			int currenHourAngle = hourAngle * hour - startTimeOffsetDegrees;
			int futureHourAngle = hourAngle * (hour + 1) - startTimeOffsetDegrees;

			int finalHourAngle = (int) Mathf.LerpAngle (currenHourAngle, futureHourAngle, minute / 60.0f);

			hours.transform.localEulerAngles = new Vector3 (localHourAngles.x, localHourAngles.y, finalHourAngle);
		}
	}
}
