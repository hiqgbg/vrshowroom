﻿using UnityEngine;
using System.Collections;

public class UnityMoviePlayer : MonoBehaviour
{
	public MovieTexture[] movies;
	public AudioClip[] audioClips;

	public bool autostart = false;
	public bool continuousPlay = false;
	public bool castLight = false;

	private bool playing = false;
	private int currentlyPlaying = 0;

	private AudioSource audioSource;
	private Renderer filmRenderer;
	private bool paused = false;

	public void Start()
	{
		filmRenderer = GetComponent<Renderer>();
		audioSource = GetComponent<AudioSource>();

		if (continuousPlay)
		{
			InvokeRepeating ("ContinuousPlay", 0.01f, 3.0f);
		}

		SetFilmClipAndAudio(currentlyPlaying);

		if (autostart)
		{
			play();
		}                                    
	}

	public void play()
	{
		filmRenderer.material.SetColor("_Color", Color.gray);
		filmRenderer.material.SetColor("_EmissionColor", Color.white);

		movies[currentlyPlaying].Play();
		audioSource.Play();

		if(castLight && !playing)
		{
			InvokeRepeating("UpdateMaterials", 0.01f, 0.05f);
		}

		playing = true;
		paused = false;

	}

	public void pause()
	{
		if (playing)
		{
			movies [currentlyPlaying].Pause();
			audioSource.Pause();
			paused = true;
		} else
		{
			play();
		}
	}

	public void forward()
	{
		movies[currentlyPlaying].Stop();
		audioSource.Stop();

		if (currentlyPlaying + 1 >= movies.Length) 
		{
			currentlyPlaying = 0;
		}
		else
		{
			currentlyPlaying++;
		}

		SetFilmClipAndAudio(currentlyPlaying);
	}

	public void rewind()
	{
		movies[currentlyPlaying].Stop();
		audioSource.Stop();

		if (currentlyPlaying - 1 < 0)
			currentlyPlaying = movies.Length - 1;
		else
		{
			currentlyPlaying--;
		}

		SetFilmClipAndAudio(currentlyPlaying);
	}

	public void stop()
	{
		CancelInvoke();
		movies[currentlyPlaying].Stop();
		audioSource.Stop();

		DynamicGI.UpdateMaterials(filmRenderer);
		playing = false;
	}

	public bool isPlaying()
	{
		return playing;
	}

	void SetFilmClipAndAudio(int clipIndex)
	{
		filmRenderer.material.SetTexture("_MainTex", movies[clipIndex]);
		filmRenderer.material.SetTexture("_EmissionMap", movies[clipIndex]);
		audioSource.clip = audioClips[clipIndex];
	}

	void ContinuousPlay()
	{
		if(!movies[currentlyPlaying].isPlaying &&
			!paused)
		{
			if(continuousPlay)
			{
				forward();
				play();
			}
			else
			{
				stop();
			}
		}
	}

	void UpdateMaterials()
	{
		if(castLight && movies[currentlyPlaying].isPlaying)
		{
			DynamicGI.UpdateMaterials(filmRenderer);
		} else if(!castLight)
		{
			CancelInvoke();
		}
	}
}

