﻿using UnityEngine;
using System;
using System.IO;

public class MouseMoveCamera : MonoBehaviour
{

    private Vector3 right = new Vector3(1, 0, 0);
    private Vector3 forward = new Vector3(0, 0, 1);

    private float horizontal = 0;
    private float vertical = 0;

    public Vector3 head = new Vector3(0, 1.56f, 0);

    private Vector3 pos = new Vector3();
    //private Vector3 up = new Vector3(0, 1, 0);
    //private Quaternion rot = new Quaternion();

    public String dataPath = "";
    public float sampleTime = 0.1f;
    private FileStream currentSampleFile = null;
    private BinaryWriter writer = null;
    private float lastSampleTime;

    public Vector3 hitPoint;
    public Vector3 hitNormal;
    public float hitDistance;
    public String hitType;

    public Vector2 textureUV;

    private enum channelType
    {
        position = 1,
        lookat = 2
    }

    private bool heatMapMode = false;
    public Shader heatMapShader;
    public bool readSamples = false;
    public int voxelMapSize = 16;
    public Color hotColor = new Color(1, 0, 0, 0);
    public Color coldColor = new Color(0, 0, 1, 0);
    public int gaussianRadius = 3;

    private Texture3D heatMapIndexes;
    private Texture3D heatMapVoxels;

    // Use this for initialization
    void Start()
    {
        Camera camera = gameObject.GetComponent<Camera>();
        if (camera == null)
        {
            Debug.LogError("Script must be assigned to a camera.");
        }
        lastSampleTime = Time.time;
        pos = transform.position;
        if (dataPath != "")
        {
            if (readSamples)
            {
                // Read samples from disk and build voxel maps.
                String[] dataFiles = Directory.GetFiles(dataPath, "HiQData*.bin");
                // Collect data
                foreach (var file in dataFiles)
                {
                    ReadDataFile(file);
                }

                // Turn data into 3D texturemaps and assign to heatmap shader

            }
            else
            {
                // We output samples to disk
                String path = dataPath + "/HiQData" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".bin";
                currentSampleFile = File.Create(path);
                if (currentSampleFile != null)
                {
                    writer = new BinaryWriter(currentSampleFile);
                    WriteHeader();
                }
            }
        }
    }

    struct Sampler
    {
        public int channelNum;
        public channelType[] channelTypes;
        public int[] channelSize;
        public int mask;
        public float sampleTime;
        public Vector3 pos;
        public Vector3 lookat;
    }

    private void ReadDataFile(string file)
    {
        try
        {
            FileStream f = File.OpenRead(file);

            using (BinaryReader reader = new BinaryReader(f))
            {
                // Get header
                byte[] magic;
                int version;
                int channelNum;

                magic = reader.ReadBytes(4);
                version = reader.ReadInt32();
                channelNum = reader.ReadInt32();

                // Check version
                if (version < 1 || version > 1)
                {
                    // Versions should only change when the format have changed in such way that the reader will fail if reading the file.
                    // Changing channels should not change versions
                    throw new IOException("Unknown file version: " + file);
                }

                // Check magic
                if (magic[0] != (byte)'H' || magic[1] != (byte)'i' || magic[2] != (byte)'Q' || magic[3] != (byte)'!')
                {
                    throw new IOException("Invalid file: " + file);
                }

                // Prepare sampler
                Sampler sampler = new Sampler();
                sampler.channelNum = channelNum;

                // Get channel description
                sampler.channelTypes = new channelType[channelNum];
                sampler.channelSize = new int[channelNum];
                for (int channel = 0; channel < channelNum; ++channel)
                {
                    sampler.channelTypes[channel] = (channelType)reader.ReadInt32();
                    sampler.channelSize[channel] = reader.ReadInt32();
                }


                if (reader.BaseStream.Position != reader.BaseStream.Length)
                {
                    long dataPosition = reader.BaseStream.Position;

                    // Pass 1: get sample bounds
                    Bounds bounds;
                    ReadSample(reader, ref sampler);
                    bounds = new Bounds(sampler.pos, Vector3.zero);
                    while (reader.BaseStream.Position != reader.BaseStream.Length)
                    {
                        ReadSample(reader, ref sampler);
                        bounds.Encapsulate(sampler.pos - head);
                    }

                    reader.BaseStream.Position = dataPosition;

                    // Pass 2: Collect voxeldata
                    Vector3 min = new Vector3(Mathf.Floor(bounds.center.x - bounds.size.x), Mathf.Floor(bounds.center.y - bounds.size.y), Mathf.Floor(bounds.center.z - bounds.size.z));
                    Vector3 max = new Vector3(Mathf.Ceil(bounds.center.x + bounds.size.x), Mathf.Ceil(bounds.center.y + bounds.size.y), Mathf.Ceil(bounds.center.z + bounds.size.z));
                    int dimX = (int)(max.x - min.x);
                    int dimY = (int)(max.y - min.y);
                    int dimZ = (int)(max.z - min.z);

                    float[,,][,,] voxelMap = new float[dimZ, dimY, dimX][,,];

                    ReadSample(reader, ref sampler);
                    Vector3 lastPos = (sampler.pos - head) - min;
                    reader.BaseStream.Position = dataPosition;

                    while (reader.BaseStream.Position != reader.BaseStream.Length)
                    {
                        ReadSample(reader, ref sampler);
                        Vector3 pos = (sampler.pos - head) - min;
                        PlotVoxelLine(voxelMap, lastPos, pos);
                        lastPos = pos;
                    }

                    // Gaussian filter
                    voxelMap = GaussianFilter(voxelMap);

                    // Pass 3: Build minimal voxel texture and voxel index texture.

                    // Collect non empty voxel fields
                    int nonEmptyFields = 1; // We also need to store an empty field to avoid shader branching.
                    for (int iz = 0; iz < dimZ; ++iz)
                    {
                        for (int iy = 0; iy < dimY; ++iy)
                        {
                            for (int ix = 0; ix < dimX; ++ix)
                            {
                                if (voxelMap[iz, iy, ix] != null)
                                {
                                    nonEmptyFields++;
                                }
                            }
                        }
                    }
                    Debug.LogWarning("nonEmptyFields: " + nonEmptyFields.ToString());
                    int voxelMem = nonEmptyFields * voxelMapSize * voxelMapSize * voxelMapSize * sizeof(float);
                    Debug.LogWarning("Voxelmem: " + voxelMem.ToString() );

                    // Find a memory saving texture size to store voxels. Must not be less than voxelMapSize.
                    int maxAllowed = 2048 / voxelMapSize;
                    int foundX = maxAllowed;
                    int foundY = maxAllowed;
                    int foundZ = maxAllowed;
                    bool foundOne = false;
                    for (int sizeX = log2Ceil(nonEmptyFields, maxAllowed); sizeX > 0; sizeX = sizeX >> 1)
                    {
                        for (int sizeY = log2Ceil(nonEmptyFields / sizeX, maxAllowed); sizeY > 0; sizeY = sizeY >> 1)
                        {
                            int sizeZ = log2Ceil(nonEmptyFields / (sizeX * sizeY), 0);
                            if (sizeZ > maxAllowed)
                            {
                                continue;
                            }
                            if (((long)foundX * (long)foundY * (long)foundZ) > ((long)sizeX * (long)sizeY * (long)sizeZ))
                            {
                                foundX = sizeX;
                                foundY = sizeY;
                                foundZ = sizeZ;
                                foundOne = true;
                            }
                        }
                    }
                    if (!foundOne)
                    {
                        throw new IOException("Not able to find a valid voxel texture size!");
                    }
                    Debug.LogWarning("Best size: " + foundX.ToString() + ", " + foundY.ToString() + ", " + foundZ.ToString());
                    int texMem = foundX * foundY * foundZ * voxelMapSize * voxelMapSize * voxelMapSize * sizeof(float);
                    Debug.LogWarning("Texmem: " + texMem.ToString());

                    int indexMapSizeX = log2Ceil(dimX, 0);
                    int indexMapSizeY = log2Ceil(dimY, 0);
                    int indexMapSizeZ = log2Ceil(dimZ, 0);

                    heatMapIndexes = new Texture3D(indexMapSizeX, indexMapSizeY, indexMapSizeZ, TextureFormat.RGB24, false);
                    heatMapVoxels = new Texture3D(foundX * voxelMapSize, foundY * voxelMapSize, foundZ * voxelMapSize, TextureFormat.RGB24, false);

                    Color32[] indexMap = new Color32[indexMapSizeX * indexMapSizeY * indexMapSizeZ];
                    for (int i = indexMapSizeX * indexMapSizeY * indexMapSizeZ; --i >= 0;)
                    {
                        indexMap[i] = new Color32(0, 0, 0, 255);
                    }

                    Color32[] voxelPixels = new Color32[foundX * voxelMapSize * foundY * voxelMapSize * foundZ * voxelMapSize];
                    for (int i = voxelPixels.Length; --i >= 0;)
                    {
                        voxelPixels[i] = new Color32(0, 0, 0, 1);
                    }

                    int currentVoxelMap = 1;
                    for (int iz = 0; iz < dimZ; ++iz)
                    {
                        for (int iy = 0; iy < dimY; ++iy)
                        {
                            for (int ix = 0; ix < dimX; ++ix)
                            {
                                if (voxelMap[iz, iy, ix] != null)
                                {
                                    // Store voxels in texture
                                    long r = currentVoxelMap % foundX;
                                    long g = (currentVoxelMap / foundX) % foundY;
                                    long b = currentVoxelMap / (foundX * foundY);
                                    indexMap[((iz * indexMapSizeY) + iy) * indexMapSizeX + ix] = new Color32((byte)r, (byte)g, (byte)b, 0);
//                                    indexMap[((iz * indexMapSizeY) + iy) * indexMapSizeX + ix] = new Color32((byte)(r * 256 / foundX), (byte)(g * 256 / foundY), (byte)(b * 256 / foundZ), 0);
                                    //                                    indexMap[((iz * indexMapSizeY) + iy) * indexMapSizeX + ix] = new Color32(7, 10, 67, 163);

                                    r *= voxelMapSize;
                                    g *= voxelMapSize;
                                    b *= voxelMapSize;
                                    float[,,] map = voxelMap[iz, iy, ix];
                                    for (long viz = 0; viz < voxelMapSize; ++viz)
                                    {
                                        for (long viy = 0; viy < voxelMapSize; ++viy)
                                        {
                                            for (long vix = 0; vix < voxelMapSize; ++vix)
                                            {
                                                byte v = (byte)Mathf.Min(map[viz, viy, vix] * 255.0f, 255.0f);
                                                voxelPixels[((((b + viz) * (long)(foundY * voxelMapSize)) + (g + viy)) * (long)(foundX * voxelMapSize)) + (r + vix)] = new Color32(v, v, v, 255);
                                                //voxelPixels[((((b + viz) * (long)(foundY * voxelMapSize)) + (g + viy)) * (long)(foundX * voxelMapSize)) + (r + vix)] = ((map[viz, viy, vix] == 0) ? new Color32(0, 0, 0, 255) : new Color32(255, 255, 255, 255));
                                                //voxelPixels[((((b + viz) * (long)(foundY * voxelMapSize)) + (g + viy)) * (long)(foundX * voxelMapSize)) + (r + vix)] = new Color32((byte)(vix * 16), (byte)(viy * 16), (byte)(viz * 16), 0);
                                            }
                                        }
                                    }
                                    currentVoxelMap++;
                                }
                                else
                                {
                                    indexMap[((iz * indexMapSizeY) + iy) * indexMapSizeX + ix] = new Color32(0, 0, 0, 0);
                                }
                            }
                        }
                    }
                    heatMapIndexes.SetPixels32(indexMap, 0);
                    heatMapIndexes.filterMode = FilterMode.Point;
                    heatMapIndexes.wrapMode = TextureWrapMode.Clamp;
                    heatMapIndexes.Apply();
                    Shader.SetGlobalTexture("_HeatMapIndex", heatMapIndexes);

                    heatMapVoxels.SetPixels32(voxelPixels, 0);
                    heatMapVoxels.filterMode = FilterMode.Point;
                    heatMapVoxels.wrapMode = TextureWrapMode.Clamp;
                    heatMapVoxels.Apply();
                    Shader.SetGlobalTexture("_HeatMapVoxels", heatMapVoxels);

                    Shader.SetGlobalVector("_HeatMapOffset", new Vector4(min.x, min.y, min.z));
                    Shader.SetGlobalVector("_HeatMapSize", new Vector4(indexMapSizeX, indexMapSizeY, indexMapSizeZ));
                    Shader.SetGlobalVector("_VoxelScale", new Vector4((float)foundX, (float)foundY, (float)foundZ));
                    Shader.SetGlobalFloat("_VoxelSize", 1.0f / voxelMapSize);
                    Shader.SetGlobalColor("_HotColor", hotColor);
                    Shader.SetGlobalColor("_ColdColor", coldColor);
                }
            }
        }
        catch ( IOException e)
        {
            Debug.LogError(e.ToString());

        }
    }

    private float[,,][,,] GaussianFilter(float[,,][,,] voxelMap)
    {
        float[] bellCurve = ComputeGaussianKernel(1.0f);
        voxelMap = boxBlurX(voxelMap, bellCurve);
        voxelMap = boxBlurY(voxelMap, bellCurve);
        voxelMap = boxBlurZ(voxelMap, bellCurve);
        return voxelMap;
    }

    // Calculates a 1d gaussian bell shaped kernel
    float[] ComputeGaussianKernel(float inWeight)
    {
        int mem_amount = (gaussianRadius * 2) + 1;
        float[] gaussian_kernel = new float[mem_amount];

        float twoRadiusSquaredRecip = 1.0f / (2.0f * gaussianRadius * gaussianRadius);
        float sqrtTwoPiTimesRadiusRecip = 1.0f / (Mathf.Sqrt(2.0f * Mathf.PI) * gaussianRadius);
        float radiusModifier = inWeight;

        // Create Gaussian Kernel
        int r = -gaussianRadius;
        float sum = 0.0f;
        for (int i = 0; i < mem_amount; i++)
        {
            float x = r * radiusModifier;
            x *= x;
            float v = sqrtTwoPiTimesRadiusRecip * Mathf.Exp(-x * twoRadiusSquaredRecip);
            gaussian_kernel[i] = v;

            sum += v;
            r++;
        }

        // Normalize distribution
        float div = sum;
        for (int i = 0; i < mem_amount; i++)
        {
            gaussian_kernel[i] /= div;
        }

        return gaussian_kernel;
    }

    private float[,,][,,] boxBlurX(float[,,][,,] scl, float[] bellCurve)
    {
        int dimX = scl.GetLength(2) * voxelMapSize;
        int dimY = scl.GetLength(1) * voxelMapSize;
        int dimZ = scl.GetLength(0) * voxelMapSize;

        float[,,][,,] tcl = new float[scl.GetLength(0), scl.GetLength(1), scl.GetLength(2)][,,];

        int idxX;
        float[,,] map;
        for (int z = 0; z < dimZ; ++z)
        {
            int idxZ = z / voxelMapSize;
            int idx2Z = z % voxelMapSize;
            for (int y = 0; y < dimY; ++y)
            {
                int idxY = y / voxelMapSize;
                int idx2Y = y % voxelMapSize;
                for (int x = 0; x < dimX; ++x)
                {
                    float blurVal = 0;
                    for (int i = 0; i < bellCurve.Length; ++i)
                    {
                        int ix = (x - gaussianRadius) + i;
                        if (ix > 0 && ix < dimX)
                        {
                            idxX = ix / voxelMapSize;
                            map = scl[idxZ, idxY, idxX];
                            if (map != null)
                            {
                                blurVal += map[idx2Z, idx2Y, ix % voxelMapSize] * bellCurve[i];
                            }
                        }
                    }
                    idxX = x / voxelMapSize;
                    map = tcl[idxZ, idxY, idxX];
                    if (map == null)
                    {
                        map = new float[voxelMapSize, voxelMapSize, voxelMapSize];
                        Array.Clear(map, 0, map.Length);
                        tcl[idxZ, idxY, idxX] = map;
                    }
                    map[idx2Z, idx2Y, x % voxelMapSize] = blurVal;
                }
            }
        }

        return tcl;
    }

    private float[,,][,,] boxBlurY(float[,,][,,] scl, float[] bellCurve)
    {
        int dimX = scl.GetLength(2) * voxelMapSize;
        int dimY = scl.GetLength(1) * voxelMapSize;
        int dimZ = scl.GetLength(0) * voxelMapSize;

        float[,,][,,] tcl = new float[scl.GetLength(0), scl.GetLength(1), scl.GetLength(2)][,,];

        int idxY;
        float[,,] map;
        for (int z = 0; z < dimZ; ++z)
        {
            int idxZ = z / voxelMapSize;
            int idx2Z = z % voxelMapSize;
            for (int x = 0; x < dimX; ++x)
            {
                int idxX = x / voxelMapSize;
                int idx2X = x % voxelMapSize;
                for (int y = 0; y < dimY; ++y)
                {
                    float blurVal = 0;
                    for (int i = 0; i < bellCurve.Length; ++i)
                    {
                        int iy = (y - gaussianRadius) + i;
                        if (iy > 0 && iy < dimY)
                        {
                            idxY = iy / voxelMapSize;
                            map = scl[idxZ, idxY, idxX];
                            if (map != null)
                            {
                                blurVal += map[idx2Z, iy % voxelMapSize, idx2X] * bellCurve[i];
                            }
                        }
                    }
                    idxY = y / voxelMapSize;
                    map = tcl[idxZ, idxY, idxX];
                    if (map == null)
                    {
                        map = new float[voxelMapSize, voxelMapSize, voxelMapSize];
                        Array.Clear(map, 0, map.Length);
                        tcl[idxZ, idxY, idxX] = map;
                    }
                    map[idx2Z, y % voxelMapSize, idx2X] = blurVal;
                }
            }
        }

        return tcl;
    }

    private float[,,][,,] boxBlurZ(float[,,][,,] scl, float[] bellCurve)
    {
        int dimX = scl.GetLength(2) * voxelMapSize;
        int dimY = scl.GetLength(1) * voxelMapSize;
        int dimZ = scl.GetLength(0) * voxelMapSize;

        float[,,][,,] tcl = new float[scl.GetLength(0), scl.GetLength(1), scl.GetLength(2)][,,];

        int idxZ;
        float[,,] map;
        for (int x = 0; x < dimX; ++x)
        {
            int idxX = x / voxelMapSize;
            int idx2X = x % voxelMapSize;
            for (int y = 0; y < dimY; ++y)
            {
                int idxY = y / voxelMapSize;
                int idx2Y = y % voxelMapSize;
                for (int z = 0; z < dimZ; ++z)
                {
                    float blurVal = 0;
                    for (int i = 0; i < bellCurve.Length; ++i)
                    {
                        int iz = (z - gaussianRadius) + i;
                        if (iz > 0 && iz < dimZ)
                        {
                            idxZ = iz / voxelMapSize;
                            map = scl[idxZ, idxY, idxX];
                            if (map != null)
                            {
                                blurVal += map[iz % voxelMapSize, idx2Y, idx2X] * bellCurve[i];
                            }
                        }
                    }
                    idxZ = z / voxelMapSize;
                    map = tcl[idxZ, idxY, idxX];
                    if (map == null)
                    {
                        map = new float[voxelMapSize, voxelMapSize, voxelMapSize];
                        Array.Clear(map, 0, map.Length);
                        tcl[idxZ, idxY, idxX] = map;
                    }
                    map[z % voxelMapSize, idx2Y, idx2X] = blurVal;
                }
            }
        }

        return tcl;
    }


    private void PlotVoxelLine(float[,,][,,] voxelMap, Vector3 lastPos, Vector3 pos)
    {
        int dist = Mathf.CeilToInt(Mathf.Abs(pos.x - lastPos.x) * voxelMapSize);
        int disto = Mathf.CeilToInt(Mathf.Abs(pos.y - lastPos.y) * voxelMapSize);
        if (dist < disto) { dist = disto; }
        disto = Mathf.CeilToInt(Mathf.Abs(pos.z - lastPos.z) * voxelMapSize);
        if (dist < disto) { dist = disto; }
        float step = 1.0f / (float)dist;
        
        for (float t = 0; t < 1.0f; t += step)
        {

            PlotVoxel(voxelMap, (lastPos * t) + (pos * (1.0f - t)));
        }
    }

    private void PlotVoxel(float[,,][,,] voxelMap, Vector3 pos)
    {
        int indexX = Mathf.FloorToInt(pos.x);
        int indexY = Mathf.FloorToInt(pos.y);
        int indexZ = Mathf.FloorToInt(pos.z);
        int voxelX = (int)((pos.x - (float)indexX) * voxelMapSize);
        int voxelY = (int)((pos.y - (float)indexY) * voxelMapSize);
        int voxelZ = (int)((pos.z - (float)indexZ) * voxelMapSize);
        float[,,] map = voxelMap[indexZ, indexY, indexX];
        if (map == null)
        {
            map = new float[voxelMapSize, voxelMapSize, voxelMapSize];
            Array.Clear(map, 0, map.Length);
            voxelMap[indexZ, indexY, indexX] = map;
        }
        map[voxelZ, voxelY, voxelX] += 10;
    }

    private int log2Ceil(int val, int max)
    {
        int log2Size = val;
        log2Size |= log2Size >> 1;
        log2Size |= log2Size >> 1;
        log2Size |= log2Size >> 1;
        log2Size |= log2Size >> 1;
        log2Size |= log2Size >> 1;
        log2Size += 1;
        if (max > 0)
        {
            if (log2Size > max)
            {
                return max;
            }
        }
        return log2Size;
    }

    private void ReadSample(BinaryReader reader, ref Sampler sampler)
    {
        // Read sample
        sampler.sampleTime = reader.ReadSingle();
        sampler.mask = reader.ReadInt32();

        for (int channel = 0; channel < sampler.channelNum; ++channel)
        {
            if (((sampler.mask >> channel) & 1) != 0)
            {
                switch (sampler.channelTypes[channel])
                {
                    case channelType.position:
                        sampler.pos.x = reader.ReadSingle();
                        sampler.pos.y = reader.ReadSingle();
                        sampler.pos.z = reader.ReadSingle();
                        break;
                    case channelType.lookat:
                        sampler.lookat.x = reader.ReadSingle();
                        sampler.lookat.y = reader.ReadSingle();
                        sampler.lookat.z = reader.ReadSingle();
                        break;
                    default:
                        reader.ReadBytes(sampler.channelSize[channel]);
                        break;
                }
            }
        }
    }

    private void WriteHeader()
    {
        if (writer != null)
        {
            var magic = new byte[] { (byte)'H', (byte)'i', (byte)'Q', (byte)'!' };
            int version = 1;
            int channelNum = 2;
            writer.Write(magic);
            writer.Write(version);
            writer.Write(channelNum);
            writer.Write((int)channelType.position);
            writer.Write((int)sizeof(float) * 3);
            writer.Write((int)channelType.lookat);
            writer.Write((int)sizeof(float) * 3);
        }
    }

    void OnApplicationQuit()
    {
        if (writer != null)
        {
            writer.Close();
        }
        else if (currentSampleFile != null)
        {
            currentSampleFile.Close();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp("h"))
        {
            heatMapMode = !heatMapMode;

            Camera camera = gameObject.GetComponent<Camera>();
            if (heatMapMode)
            {
                camera.SetReplacementShader(heatMapShader, "");
            }
            else
            {
                camera.ResetReplacementShader();
            }
            /*
            Collider[] allColliders = FindObjectsOfType<Collider>();
            foreach (Collider coll in allColliders)
            {
                //Debug.Log(coll.name);
                Renderer render = coll.gameObject.GetComponent<Renderer>();
                if (render != null)
                {
                    render.enabled = !heatMapMode;
                }
            }
            */
        }

        bool mouseMove = false;
        bool keyMove = false;
        if (Input.GetAxis("Fire1") != 0)
        {
            mouseMove = true;
            float deltaTime = Time.deltaTime * 100;
            horizontal -= deltaTime * Input.GetAxis("Mouse X");
            vertical += deltaTime * Input.GetAxis("Mouse Y");
            if (vertical < -90)
            {
                vertical = -90;
            }
            if (vertical > 90)
            {
                vertical = 90;
            }
            if (horizontal < -360)
            {
                horizontal += 360;
            }
            if (horizontal > 360)
            {
                horizontal -= 360;
            }
        }
        {
            float deltaTime = Time.deltaTime * 10;
            right = transform.right;
            forward = transform.forward;
            right.y = 0;
            right.Normalize();
            right *= deltaTime;
            forward.y = 0;
            forward.Normalize();
            forward *= deltaTime;
            if (Input.GetKey("w"))
            {
                keyMove = true;
                pos += forward;
            }
            if (Input.GetKey("a"))
            {
                keyMove = true;
                pos -= right;
            }
            if (Input.GetKey("s"))
            {
                keyMove = true;
                pos -= forward;
            }
            if (Input.GetKey("d"))
            {
                keyMove = true;
                pos += right;
            }
        }
        if (keyMove || mouseMove)
        {
            transform.position = pos + head;
            transform.rotation = Quaternion.Euler(vertical, horizontal, 0);
            WriteSample();

            RaycastHit hitInfo;
            if (Physics.Raycast(transform.position, transform.forward, out hitInfo))
            {
                //hitBarycentric = hitInfo.barycentricCoordinate;
                hitPoint = hitInfo.point;
                hitNormal = hitInfo.normal;
                hitDistance = hitInfo.distance;
                hitType = hitInfo.collider.GetType().ToString();
                Vector3 localHitPoint = hitInfo.collider.transform.InverseTransformPoint(hitInfo.point);
                Vector3 localNormal = hitInfo.collider.transform.InverseTransformVector(hitInfo.normal);

                if (hitInfo.collider.GetType() == typeof(SphereCollider))
                {
                    // Calculate latlong uv coordinates for sphere
                    textureUV.x = (Mathf.Atan2(localNormal.z, localNormal.x) + Mathf.PI) / (Mathf.PI * 2); // longitude 0-1
                    textureUV.y = (localNormal.y + 1) / 2; //Mathf.Atan(hitInfo.normal.y); // latitude 0-1
                    // Texels are wider at the poles than at the equator.
                    float longitudeRadius = Mathf.Sqrt(1 - (localNormal.y * localNormal.y));
                    float texelWidth = 1.0f;
                    if (longitudeRadius > 0)
                    {
                        texelWidth = 1 / longitudeRadius;
                    }


                }
                else if (hitInfo.collider.GetType() == typeof(BoxCollider))
                {
                    // Calculate uv coordinates for box.
                    Vector3 size = ((BoxCollider)(hitInfo.collider)).size;

                    localHitPoint.x = (localHitPoint.x / size.x) + 0.5f;
                    localHitPoint.y = (localHitPoint.y / size.y) + 0.5f;
                    localHitPoint.z = (localHitPoint.z / size.z) + 0.5f;

                    if ((localNormal.x * localNormal.x) < 0.5f)
                    {
                        textureUV.x = localHitPoint.x;
                    }
                    else
                    {
                        textureUV.x = localHitPoint.y;
                    }
                    if ((localNormal.z * localNormal.z) < 0.5f)
                    {
                        textureUV.y = localHitPoint.z;
                    }
                    else
                    {
                        textureUV.y = localHitPoint.y;
                    }
                }
            }
        }
    }

    private void WriteSample()
    {
        // timestamp
        // bitmask for channels in current sample. Not all channels must exist for every sample.
        // current channels
        if (lastSampleTime + sampleTime <= Time.time)
        {
            lastSampleTime = Time.time;
            if (writer != null)
            {
                writer.Write(lastSampleTime);
                int mask = 3; // position and lookat
                writer.Write(mask);
                //position
                writer.Write(transform.position.x);
                writer.Write(transform.position.y);
                writer.Write(transform.position.z);
                // lookat
                writer.Write(transform.forward.x);
                writer.Write(transform.forward.y);
                writer.Write(transform.forward.z);
            }
        }
    }
}