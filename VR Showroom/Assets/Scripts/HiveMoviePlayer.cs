using UnityEngine;
using System.Collections;
using System;
using System.Runtime.InteropServices;
using System.IO;

public class HiveMoviePlayer : MonoBehaviour {
	
	[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
	private delegate void DebugPrintDelegate(string txt);

	private static DebugPrintDelegate s_debugDelegate;

	private static void UnityDebugFunction(string txt)
	{
		Debug.Log (txt);
	}

	#if UNITY_IPHONE && !UNITY_EDITOR
	[DllImport ("__Internal")]
	#else
	[DllImport("HiveMoviePlayer")]
	#endif
	private static extern void SetDebugFunction(IntPtr fp);

	// the rest should look like above if we want it for iphone
	[DllImport("HiveMoviePlayer")]
	private static extern IntPtr GetRenderEventFunc();

	[DllImport ("HiveMoviePlayer")]
	private static extern int NewOggDecoder(IntPtr fileName);

	[DllImport ("HiveMoviePlayer")]
	private static extern void SetFileName(int id, IntPtr fileName);

	[DllImport ("HiveMoviePlayer")]
	private static extern void SetMovieTextures(int id, System.IntPtr texture, System.IntPtr uTexture, System.IntPtr vTexture);

	[DllImport ("HiveMoviePlayer")]
	private static extern int GetVideoWidth(int id);

	[DllImport ("HiveMoviePlayer")]
	private static extern int GetVideoHeight(int id);

	[DllImport ("HiveMoviePlayer")]
	private static extern int GetChromaWidth(int id);

	[DllImport ("HiveMoviePlayer")]
	private static extern int GetChromaHeight(int id);

	[DllImport ("HiveMoviePlayer")]
	private static extern float GetLength(int id);

	[DllImport ("HiveMoviePlayer")]
	private static extern void PlayMovie(int id);

	[DllImport ("HiveMoviePlayer")]
	private static extern void StopMovie(int id);

	[DllImport ("HiveMoviePlayer")]
	private static extern bool IsMovieDone(int id);

	[DllImport ("HiveMoviePlayer")]
	private static extern void SetAudioTime(int id, float pos);

	[DllImport ("HiveMoviePlayer")]
	private static extern void DeleteMovie(int id);

	public string MoviePath;

	public AudioClip MovieAudio;
	public AudioSource MovieAudioSource;

	public bool CastLight = false;
	public float LightUpdateFrequency = 0.5f;

	private int mMovieDecoderId = 0;
	private float mPosition = 0;
	private bool mPaused = true;

	private Texture2D yTex;
	private Texture2D cbTex;
	private Texture2D crTex;
	private RenderTexture rgbTex;
	private Material mConvertYCbCrMaterial;
	private Renderer mRenderer;

	private Coroutine mUpdateLoop;

	void Awake()
	{
		mRenderer = GetComponent<Renderer>();
	}

	// todo move to awake
	void Start()
	{
		if (s_debugDelegate == null) 
		{
			s_debugDelegate = new DebugPrintDelegate (UnityDebugFunction);

			// Convert callback_delegate into a function pointer that can be
			// used in unmanaged code.
			IntPtr intptr_delegate = Marshal.GetFunctionPointerForDelegate(s_debugDelegate);

			// Call the API passing along the function pointer.
			SetDebugFunction(intptr_delegate);
		}
		
		if (mMovieDecoderId == 0) 
		{
			InitializeMovieAndTextures();

			if (MovieAudioSource != null) 
			{
				MovieAudioSource.clip = MovieAudio;
			}
		}
	}

	void OnDestroy()
	{
		DeleteMovie(mMovieDecoderId);
		mMovieDecoderId = 0;
	}

	private void SetRenderTextureSize(int width, int height)
	{
		if (rgbTex.width == width && rgbTex.height == height)
			return;

		if (RenderTexture.active == rgbTex)
			RenderTexture.active = null;
		
		rgbTex.Release();

		rgbTex.width = Math.Max(1, width);
		rgbTex.height = Math.Max(1, height);
	}

	private void InitializeMovieAndTextures()
	{
		Debug.AssertFormat (mMovieDecoderId == 0, "HiveMoviePlayer.InitializeMovieAndTextures called twice");

		IntPtr fileNamePtr = Marshal.StringToHGlobalAnsi (MoviePath);
		mMovieDecoderId = NewOggDecoder (fileNamePtr);
		Marshal.FreeHGlobal (fileNamePtr);
		
		int movieHeight  = GetVideoHeight(mMovieDecoderId);
		int movieWidth   = GetVideoWidth(mMovieDecoderId);
		int chromaWidth  = GetChromaWidth(mMovieDecoderId);
		int chromaHeight = GetChromaHeight(mMovieDecoderId);

		// Create the textures
		yTex  = new Texture2D(movieWidth, movieHeight, TextureFormat.Alpha8, false);
		cbTex = new Texture2D(chromaWidth, chromaHeight, TextureFormat.Alpha8, false);
		crTex = new Texture2D(chromaWidth, chromaHeight, TextureFormat.Alpha8, false);

		mConvertYCbCrMaterial = new Material(Shader.Find("Hidden/ycbcr_to_rgb"));
		mConvertYCbCrMaterial.SetTexture("_yTex", yTex);
		mConvertYCbCrMaterial.SetTexture("_cbTex", cbTex);
		mConvertYCbCrMaterial.SetTexture("_crTex", crTex);
		rgbTex = new RenderTexture (Math.Max(1, movieWidth), Math.Max(1, movieHeight), 0, RenderTextureFormat.ARGB32);
		if (CastLight) 
		{
			mRenderer.material.SetTexture ("_EmissionMap", rgbTex);
		}
		else 
		{
			mRenderer.material.mainTexture = rgbTex;
		}

		// Pass texture pointer to the plugin
		SetMovieTextures(mMovieDecoderId, yTex.GetNativeTexturePtr(), cbTex.GetNativeTexturePtr(), crTex.GetNativeTexturePtr());
	}

	private IEnumerator CallPluginAtEndOfFrames()
	{
		float lightUpdate = 0;
		while (true) 
		{
			// Wait until all frame rendering is done
			yield return new WaitForEndOfFrame();

			mPosition += Time.deltaTime;
			lightUpdate += Time.deltaTime;

			SetAudioTime(mMovieDecoderId, CurrentPosition());

			GL.IssuePluginEvent(GetRenderEventFunc(), mMovieDecoderId);
			Graphics.Blit(yTex, rgbTex, mConvertYCbCrMaterial); //todo: only blit when there is a change in yTex

			if (CastLight && lightUpdate > LightUpdateFrequency) 
			{
				lightUpdate = 0;
				DynamicGI.UpdateMaterials(mRenderer);
			}
		}
	}

	private void SetScreenOn(bool b)
	{
		Color c = b ? Color.white : Color.black;
		Texture t = b ? rgbTex : null;

		if (CastLight)
		{
			mRenderer.material.SetColor("_EmissionColor", c);
			mRenderer.material.SetTexture("_EmissionMap", t);
		}
		else
		{
			mRenderer.material.color = c;
			mRenderer.material.mainTexture = t;
		}
	}

	private void StartLoop()
	{
		mPaused = false;
		if (mUpdateLoop == null)
		{
			mUpdateLoop = StartCoroutine(CallPluginAtEndOfFrames());
		}
	}

	private void StopLoop()
	{
		mPaused = true;
		if (mUpdateLoop != null)
		{
			StopCoroutine(mUpdateLoop);
			mUpdateLoop = null;
		}
	}

	public void Play()
	{
		Stop();
		mPosition = 0;
		if (MovieAudioSource != null) 
		{
			MovieAudioSource.Play();
		}
		SetScreenOn(true);
		PlayMovie(mMovieDecoderId);
		StartLoop();
	}

	public void Pause()
	{
		StopLoop();

		if (MovieAudioSource != null) 
		{
			MovieAudioSource.Pause();
		}
		if (CastLight)
		{
			DynamicGI.UpdateMaterials(mRenderer);
		}
	}

	public void Continue()
	{
		if (MovieAudioSource != null) 
		{
			MovieAudioSource.UnPause();
		}
		StartLoop();
	}

	public void Stop()
	{
		StopLoop();
		mPosition = 0;
		if (MovieAudioSource != null) 
		{
			MovieAudioSource.Stop();
		}
		StopMovie(mMovieDecoderId);
		SetScreenOn(false);
		if (CastLight) 
		{
			DynamicGI.UpdateMaterials(mRenderer);
		}
	}

	public void SetMovie(string fileName, AudioClip audio)
	{
		MoviePath = fileName;
		MovieAudio = audio;

		if (MovieAudioSource != null) 
		{
			MovieAudioSource.clip = MovieAudio;
		}

		if (mMovieDecoderId == 0) 
		{
			InitializeMovieAndTextures();
			return;
		}

		IntPtr fileNamePtr = Marshal.StringToHGlobalAnsi(MoviePath);
		SetFileName(mMovieDecoderId, fileNamePtr);
		Marshal.FreeHGlobal(fileNamePtr);
		
		int movieHeight  = GetVideoHeight(mMovieDecoderId);
		int movieWidth   = GetVideoWidth(mMovieDecoderId);
		int chromaWidth  = GetChromaWidth(mMovieDecoderId);
		int chromaHeight = GetChromaHeight(mMovieDecoderId);

		if (yTex.width != movieWidth || yTex.height != movieHeight) 
		{
			yTex.Resize(movieWidth, movieHeight);
			yTex.Apply();
		}
		if (cbTex.width != chromaHeight || cbTex.height != chromaHeight) 
		{
			cbTex.Resize(chromaWidth, chromaHeight);
			crTex.Resize(chromaWidth, chromaHeight);
			cbTex.Apply();
			crTex.Apply();
		}
		
		SetMovieTextures(mMovieDecoderId, yTex.GetNativeTexturePtr(), cbTex.GetNativeTexturePtr(), crTex.GetNativeTexturePtr());
		SetRenderTextureSize(movieWidth, movieHeight);
	}

	public bool IsPaused()
	{
		return mPaused;
	}

	public bool IsDone()
	{
		return IsMovieDone(mMovieDecoderId);
	}

	public float CurrentPosition()
	{
		if (MovieAudioSource != null && MovieAudio != null)
			return (float)MovieAudioSource.timeSamples / (float)MovieAudio.frequency;
		else
			return mPosition;
	}

	public float Length()
	{
		if (MovieAudioSource != null && MovieAudio != null)
			return MovieAudio.length;
		else
			return GetLength(mMovieDecoderId);
	}
	
	public void OnApplicationQuit()
	{
		Stop();
	}
}
