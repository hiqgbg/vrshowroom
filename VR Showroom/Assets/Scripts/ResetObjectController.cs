﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ResetObjectController : MonoBehaviour {
    public void ResetAllPositions()
    {
		GetComponent<ResetObject> ().ResetAllPositions();
    }

	void Update()
	{
		if (Input.GetKeyUp (KeyCode.R))
		{
			ResetAllPositions();
		}
	}
}
