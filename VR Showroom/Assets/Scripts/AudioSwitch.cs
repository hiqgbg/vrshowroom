﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections;
using System.Collections.Generic;

public class AudioSwitch : MonoBehaviour {

	public AudioSource[] audioSources;

	private Dictionary<AudioMixerGroup, List<AudioSource>> categoryMap = new Dictionary<AudioMixerGroup, List<AudioSource>>();
	// Use this for initialization
	void Start () {
		foreach (AudioSource source in audioSources)
		{
			List<AudioSource> list;
			Debug.LogWarning (source.outputAudioMixerGroup);
			if (categoryMap.TryGetValue (source.outputAudioMixerGroup, out list))
			{
				list.Add (source);
		
			} else
			{	
				List<AudioSource> newList = new List<AudioSource> ();
				newList.Add (source);
				categoryMap.Add (source.outputAudioMixerGroup, newList);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space))
		{
			foreach (KeyValuePair<AudioMixerGroup, List<AudioSource>> entry in categoryMap)
			{
				Debug.LogWarning (entry.Key.name);

				foreach (AudioSource source in entry.Value)
				{
					Debug.LogWarning (source.name);
				}
			}
		}
	}

	public List<string> GetCategories()
	{
		List<string> categories = new List<string> ();
		foreach (KeyValuePair<AudioMixerGroup, List<AudioSource>> entry in categoryMap)
		{
			categories.Add (entry.Key.name);
		}

		return categories;
	}
}
