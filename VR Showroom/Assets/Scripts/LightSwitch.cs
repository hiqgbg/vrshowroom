﻿using UnityEngine;
using System.Collections;

public class LightSwitch : MonoBehaviour {
	public float fadeTime = 2;

	public Light sunLight;
	public Light[] dayLights;

	public Light[] lights;

	public ReflectionProbe[] probes;

	bool m_day = true;
	float m_dayValue = 1.0f;
	float[] m_LightIntensities;
	float[] m_DayLightIntensities;

	// Use this for initialization
	void Start ()
	{
		m_LightIntensities = new float[lights.Length];
		int i = 0;
		foreach (Light l in lights)
		{
			m_LightIntensities[i++] = l.intensity;
		}

		m_DayLightIntensities = new float[dayLights.Length];
		int k = 0;
		foreach (Light l in dayLights)
		{
			m_DayLightIntensities[k++] = l.intensity;
		}

		GetComponent<VRTK.VRTK_InteractableObject>().InteractableObjectTouched += new VRTK.InteractableObjectEventHandler(TouchStart);

		StartCoroutine(RenderProbesWithDelay(1.5f));

	}

	IEnumerator RenderProbesWithDelay(float delay)
	{
		//Coroutine will wait 1 second to continue         
		yield return new WaitForSeconds(delay);

		foreach (ReflectionProbe p in probes)
		{
			p.RenderProbe();
		}
	}


	// can be triggered by other light buttons
	public void ToggleDay()
	{
		m_day = !m_day;
		// enable all lights as we fade them
		sunLight.enabled = true;
		foreach (Light l in lights)
		{
			l.enabled = true;
		}

		foreach (Light l in dayLights)
		{
			l.enabled = true;
		}

		if (m_day)
		{
			StartCoroutine(FadeToLight());
		}
		else
		{
			StartCoroutine(FadeToDark());
		}

	}

	void TouchStart(object sender, VRTK.InteractableObjectEventArgs e)
	{
		ToggleDay();
	}

	void SetDayValue(float dv)
	{
		m_dayValue = Mathf.Clamp01(dv);
		sunLight.intensity = m_dayValue;

		int k = 0;
		foreach (Light l in dayLights)
		{
			l.intensity = m_dayValue * m_LightIntensities[k++];
		}

		int i = 0;
		foreach (Light l in lights)
		{
			l.intensity = (1.0f - m_dayValue) * m_LightIntensities[i++];
		}

		foreach (ReflectionProbe p in probes)
		{
			p.intensity = m_dayValue * 0.4f + 0.6f;
		}
		RenderSettings.skybox.SetFloat("_Exposure", m_dayValue * 0.5f + 0.5f);
	}

	void SetLightsEnabled(bool enabled)
	{
		sunLight.enabled = !enabled;
		foreach (Light l in dayLights)
		{
			l.enabled = !enabled;
		}

		foreach (Light l in lights)
		{
			l.enabled = enabled;
		}
	}

	IEnumerator FadeToDark()
	{
		for (float t = 0.0f; t < fadeTime; t += Time.deltaTime)
		{
			float m_dayValue = Mathf.Lerp(0.0f, 1.0f, t/fadeTime);
			SetDayValue(1.0f - m_dayValue);
			yield return null;
		}
		//SetLightsEnabled(false);

		foreach (ReflectionProbe p in probes)
		{
			p.RenderProbe();
		}
	}

	IEnumerator FadeToLight()
	{
		for (float t = 0.0f; t < fadeTime; t += Time.deltaTime)
		{
			float m_dayValue = Mathf.Lerp(0.0f, 1.0f, t/fadeTime);
			SetDayValue(m_dayValue);
			yield return null;
		}
		//SetLightsEnabled(true);

		foreach (ReflectionProbe p in probes)
		{
			p.RenderProbe();
		}
	}

	void Update()
	{
		if (Input.GetKeyUp (KeyCode.L))
		{
			ToggleDay();
		}
	}
}
