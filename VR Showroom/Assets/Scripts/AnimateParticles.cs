﻿using UnityEngine;
using System.Collections;

public class AnimateParticles : MonoBehaviour {

	public float rotation = 1;
	private ParticleSystem ps;
	private Vector3 rotationVector;
	// Use this for initialization
	void Start () {
		ps = GetComponent<ParticleSystem> ();
		rotationVector = new Vector3 (0.0f, 0.0f, rotation);
	}
	
	// Update is called once per frame
	void Update () {
		ps.transform.Rotate (rotationVector);
	}
}
