﻿using UnityEngine;
using System.Text.RegularExpressions;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class TiledMoviePlayer : MonoBehaviour {

	static Dictionary<string, Rect> FixedPositions = new Dictionary<string, Rect> {

		{"center", new Rect(0.0f, 0.0f, 1.0f, 1.0f)},
		{"top",    new Rect(0.0f, 0.5f, 1.0f, 0.5f)},
		{"bottom", new Rect(0.0f, 0.0f, 1.0f, 0.5f)},
		{"left",   new Rect(0.0f, 0.0f, 0.5f, 1.0f)},
		{"right",  new Rect(0.5f, 0.0f, 0.5f, 1.0f)},
		
		{"topright",    new Rect(0.5f, 0.5f, 0.5f, 0.5f)},
		{"topleft",     new Rect(0.0f, 0.5f, 0.5f, 0.5f)},
		{"bottomright", new Rect(0.5f, 0.0f, 0.5f, 0.5f)},
		{"bottomleft",  new Rect(0.0f, 0.0f, 0.5f, 0.5f)},

	};

	public string layoutFile;

	public Rect[] positions;
	public string[] movieFolders;

	MoviePlayer[] players;
	GameObject[] objects;

	static Rect PositionFromFolderName(string folder)
	{
		folder = folder.TrimEnd ('/', '\\');
		folder = folder.ToLower ();
		folder = Path.GetFileName (folder);
		Rect rect = new Rect ();
		FixedPositions.TryGetValue(folder, out rect);
		return rect;
	}
	
	private static void GetRowColumn(int pos, int width, int height, out int row, out int column)
	{
		column = pos % width;
		row = pos / width;
	}

	private static void sort(ref int a, ref int b)
	{
		if (b < a) {
			int tmp = a;
			a = b;
			b = tmp;
		}
	}

	private void ParseLayoutFile(string file)
	{
		using (StreamReader reader = new StreamReader(file))
		{
			int width = 1;
			int height = 1;

			{
				Regex sizeRegex = new Regex (@"^\s*(\d+)\s+(\d+)\s*$");
				Match m = sizeRegex.Match(reader.ReadLine());

				if (m.Groups.Count == 3) {
					if (!int.TryParse (m.Groups [1].Value, out width)
						|| !int.TryParse (m.Groups [2].Value, out height)) {
						Debug.Log ("Invalid layout file. Couldn't parse size.");
						return;
					}
				} 
				else {
					Debug.Log ("Invalid layout file. Couldn't parse size.");
					return;
				}
			}

			int maxPosition = width * height;

			float tileWidth  = 1.0f / width;
			float tileHeight = 1.0f / height;

			Regex movieRegex = new Regex(@"^\s*(\d+)\s+(\d*)\s*(.+)$");

			List<string> folders = new List<string>();
			List<Rect> positions = new List<Rect> ();

			while (true) {

				string line = reader.ReadLine ();
				if (line == null)
					break;

				Match m = movieRegex.Match (line);

				if (m.Groups.Count == 4) {

					int posA, posB;

					if (!int.TryParse (m.Groups [1].Value, out posA)) {
						Debug.LogWarning (@"Movie layout file, invalid line (" + line + ")");
						continue;
					}
					if (!int.TryParse (m.Groups [2].Value, out posB)) {
						posB = posA;
					}

					// convert to 0 indexed
					posA--;
					posB--;

					string movieFolder = m.Groups [3].Value;
					if (movieFolder == "") {
						Debug.LogWarning (@"Movie layout file, invalid line (" + line + ")");
						continue;
					}

					if (posA > maxPosition || posB > maxPosition) {
						Debug.LogWarning ("Movie layout file, invalid postion (" +posA+","+posB+ ")");
						continue;
					}

					int minRow, maxRow, minCol, maxCol;

					GetRowColumn (posA, width, height, out minRow, out minCol);
					GetRowColumn (posB, width, height, out maxRow, out maxCol);

					sort (ref minRow, ref maxRow);
					sort (ref minCol, ref maxCol);

					folders.Add (movieFolder);
					positions.Add (Rect.MinMaxRect (minCol * tileWidth, minRow * tileHeight,
													(maxCol+1) * tileWidth, (maxRow+1) * tileHeight));
				}
			}

			movieFolders = folders.ToArray ();
			this.positions = positions.ToArray ();
		}
	}

	// Use this for initialization
	void Start () {

		if (layoutFile != "")
			ParseLayoutFile (layoutFile);

		Material mat = Resources.Load<Material> ("Materials/MovieMaterial");

		int len = movieFolders.Length;
		objects = new GameObject[len];
		players = new MoviePlayer[len];
		for (int i = 0; i < len; i++) {
			objects [i] = GameObject.CreatePrimitive (PrimitiveType.Quad);
			objects [i].transform.parent = transform;
			objects [i].GetComponent<MeshRenderer>().material = mat;

			players [i] = objects [i].AddComponent<MoviePlayer> ();
			players [i].readFromFolder = movieFolders [i];
			players [i].autostart = true;
			players [i].continuousPlay = true;

			objects[i].transform.localRotation = Quaternion.identity;
			Rect position = new Rect ();
			if (i < positions.Length) {
				position = positions [i];
			}

			objects [i].name = movieFolders [i];

			Vector2 size = position.size;
			Vector2 pos = position.min - new Vector2(0.5f, 0.5f) + (size * 0.5f);

			objects[i].transform.localPosition = new Vector3(pos.x, pos.y, -0.001f);
			objects[i].transform.localScale = new Vector3 (size.x, size.y, 1);
		}
	}
}
