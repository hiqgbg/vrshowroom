﻿using UnityEngine;
using System.Collections;
using VRTK;

public class HapticObject : VRTK_InteractableObject
{
	[Header("Haptics", order = 4)]
	public float impactMagnifier = 500;
	public float duration = 0.01f;
	public float pulseInterval = 0.01f;

    private VRTK_ControllerActions controllerActions;
    private VRTK_ControllerEvents controllerEvents;

    private float collisionForce = 0f;

	protected override void Start()
	{
		base.Start();
	}

    public float CollisionForce()
    {
        return collisionForce;
    }

	public void BaseStart()
	{
		Start ();
	}

    public override void Grabbed(GameObject grabbingObject)
    {
        base.Grabbed(grabbingObject);
        controllerActions = grabbingObject.GetComponent<VRTK_ControllerActions>();
        controllerEvents = grabbingObject.GetComponent<VRTK_ControllerEvents>();
    }

    protected override void Awake()
    {
        base.Awake();
        rb.collisionDetectionMode = CollisionDetectionMode.Continuous;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (controllerActions && controllerEvents && IsGrabbed())
        {
            collisionForce = controllerEvents.GetVelocity().magnitude * impactMagnifier;
			controllerActions.TriggerHapticPulse((ushort)collisionForce, duration, pulseInterval);
        }
        else
        {
            collisionForce = collision.relativeVelocity.magnitude * impactMagnifier;
        }
    }
}