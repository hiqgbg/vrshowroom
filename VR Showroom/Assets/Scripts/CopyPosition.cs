﻿using UnityEngine;
using System.Collections;

public class CopyPosition : MonoBehaviour {

    public Transform target;

    private Vector3 m_positionOffset;
    private Quaternion m_rotationOffset;

	// Use this for initialization
	void Start () {
        m_positionOffset = transform.position - target.position;
        m_rotationOffset = Quaternion.RotateTowards(transform.rotation, target.rotation, Mathf.Infinity);
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = target.position + m_positionOffset;
        transform.rotation = target.rotation * m_rotationOffset;
	}
}
