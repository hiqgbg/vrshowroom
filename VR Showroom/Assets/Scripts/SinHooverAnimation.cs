﻿using UnityEngine;
using System.Collections;

public class SinHooverAnimation : MonoBehaviour {

    public float offset = 0;

    Vector3 originalPosition;

	// Use this for initialization
	void Start () {
        originalPosition = transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        transform.position = originalPosition + new Vector3(0, Mathf.Sin(Time.time * 0.8f + offset) * 0.4f, 0);
	}
}
