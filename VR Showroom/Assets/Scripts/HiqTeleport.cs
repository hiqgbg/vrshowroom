﻿using UnityEngine;
using System.Collections;

public class HiqTeleport : VRTK.VRTK_BasicTeleport {

	public GameObject FixedPositions;

	// Use this for initialization
	protected override void Start () {
		base.Start ();
	}

	protected override void DoTeleport(object sender, VRTK.DestinationMarkerEventArgs e)
	{
		if (e.target &&
		    e.target.transform &&
		    e.target.transform.tag == "TeleportTrigger") 
		{
			Transform destination = e.target.transform.FindChild("Destination");
			if (destination)
				e.destinationPosition = destination.position;

			Transform particleSystem = e.target.transform.FindChild ("Particle System");
			if (particleSystem)
			{
				particleSystem.gameObject.SetActive(false);
			}
		}
		base.DoTeleport(sender, e);	
	}

	private int NumKeyPressed()
	{
		if (Input.GetKeyUp (KeyCode.Alpha1)) {
			return 0;
		}
		else if (Input.GetKeyUp (KeyCode.Alpha2)) {
			return 1;
		}
		else if (Input.GetKeyUp (KeyCode.Alpha3)) {
			return 2;
		}
		else if (Input.GetKeyUp (KeyCode.Alpha4)) {
			return 3;
		}
		else if (Input.GetKeyUp (KeyCode.Alpha5)) {
			return 4;
		}
		else if (Input.GetKeyUp (KeyCode.Alpha6)) {
			return 5;
		}
		else if (Input.GetKeyUp (KeyCode.Alpha7)) {
			return 6;
		}
		else if (Input.GetKeyUp (KeyCode.Alpha8)) {
			return 7;
		}
		else if (Input.GetKeyUp (KeyCode.Alpha9)) {
			return 8;
		}
		else if (Input.GetKeyUp (KeyCode.Alpha0)) {
			return 9;
		}
		return -1;
	}

	protected void Update()
	{
		if (FixedPositions == null)
			return;
		
		int index = NumKeyPressed ();
		if (index >= 0 && index < FixedPositions.transform.childCount) {
			Transform teleportPosition = FixedPositions.transform.GetChild (index);
			VRTK.DestinationMarkerEventArgs e = new VRTK.DestinationMarkerEventArgs();
			e.controllerIndex = uint.MaxValue;
			e.destinationPosition = teleportPosition.position;
			e.distance = Vector3.Distance(teleportPosition.position, this.transform.position);
			e.enableTeleport = true;
			e.target = teleportPosition;
			DoTeleport (this, e);
		}
	}
}
