﻿using UnityEngine;
using System.Collections;
using VRTK;

public class LoadSceneUseListener : MonoBehaviour {

	// Use this for initialization
	void Start () {

        GetComponent<VRTK_InteractableObject>().InteractableObjectTouched += new InteractableObjectEventHandler(DoInteractableTouched);
        GetComponent<VRTK_InteractableObject>().InteractableObjectUntouched += new InteractableObjectEventHandler(DoInteractableUntouched);
        GetComponent<VRTK_InteractableObject>().InteractableObjectUsed += new InteractableObjectEventHandler(DoInteractableUsed);
		GetComponent<VRTK_InteractableObject>().InteractableObjectUnused += new InteractableObjectEventHandler(DoInteractableUnused);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void DebugLogger(string action, GameObject target)
	{
		Debug.Log(action + " by an object named " + target.name);
	}


    void DoInteractableTouched(object sender, InteractableObjectEventArgs e)
    {  
    }

    void DoInteractableUntouched(object sender, InteractableObjectEventArgs e)
    {
    }

    void DoInteractableUsed(object sender, InteractableObjectEventArgs e)
	{
	}

	void DoInteractableUnused(object sender, InteractableObjectEventArgs e)
	{
		GetComponent<SteamVR_LoadLevel> ().enabled = true;
    }
}
