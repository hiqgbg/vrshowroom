﻿using UnityEngine;
using System.Collections;

// Radial Menu input from Vive Controller
[RequireComponent(typeof(HiqRadialMenu))]
public class HiqRadialMenuController : MonoBehaviour
{
	public VRTK.VRTK_ControllerEvents events;
	public BezierPointer teleportPointer;

	private bool menuActive = false;
	private HiqRadialMenu menu;
	private float currentAngle; //Keep track of angle for when we click

	void Start()
	{
		menu = GetComponent<HiqRadialMenu>();
		if (teleportPointer == null)
		{
			teleportPointer = GetComponentInParent<BezierPointer>();
		}

		if (events == null)
		{
			events = GetComponentInParent<VRTK.VRTK_ControllerEvents>();
		}
		if (events == null)
		{
			Debug.LogError("The radial menu must be a child of the controller or be set in the inspector!");
		}
		else
		{   //Start listening for controller events
			events.ApplicationMenuPressed += new VRTK.ControllerInteractionEventHandler(DoApplicationMenuPressed);
		}
	}

	private void DoApplicationMenuPressed(object sender, VRTK.ControllerInteractionEventArgs e)
	{
		ToggleApplicationMenu();
	}

	public void ToggleApplicationMenu()
	{
		menuActive = !menuActive;

		if (menuActive)
		{
			ActivateInput();
			menu.ShowMenu();
		} 
		else
		{
			DeactivateInput();
			menu.HideMenu(true);
		}
	}

	private void DoTouchpadClicked(object sender, VRTK.ControllerInteractionEventArgs e)
	{
		menu.ClickButton(currentAngle);
	}

	private void DoTouchpadUnclicked(object sender, VRTK.ControllerInteractionEventArgs e)
	{
		menu.UnClickButton(currentAngle);
	}

	private void DoTouchpadTouched(object sender, VRTK.ControllerInteractionEventArgs e)
	{
		menu.ShowMenu();
	}

	private void DoTouchpadUntouched(object sender, VRTK.ControllerInteractionEventArgs e)
	{
		menu.StopTouching();
		menu.HideMenu(false);
	}

	//Touchpad finger moved position
	private void DoTouchpadAxisChanged(object sender, VRTK.ControllerInteractionEventArgs e)
	{
		//Convert Touchpad Vector2 to Angle (0 to 360)
		float angle = Mathf.Atan2(e.touchpadAxis.y, e.touchpadAxis.x) * Mathf.Rad2Deg;
		angle = 90.0f - angle;
		if (angle < 0)
		{
			angle += 360.0f;
		}
		currentAngle = 360 - angle;

		menu.HoverButton(currentAngle);
	}

	private void ActivateInput()
	{
		events.TouchpadPressed += new VRTK.ControllerInteractionEventHandler(DoTouchpadClicked);
		events.TouchpadReleased += new VRTK.ControllerInteractionEventHandler(DoTouchpadUnclicked);
		events.TouchpadTouchStart += new VRTK.ControllerInteractionEventHandler(DoTouchpadTouched);
		events.TouchpadTouchEnd += new VRTK.ControllerInteractionEventHandler(DoTouchpadUntouched);
		events.TouchpadAxisChanged += new VRTK.ControllerInteractionEventHandler(DoTouchpadAxisChanged);

		teleportPointer.enabled = false;
	}

	private void DeactivateInput()
	{
		events.TouchpadPressed -= DoTouchpadClicked;
		events.TouchpadReleased -= DoTouchpadUnclicked;
		events.TouchpadTouchStart -= DoTouchpadTouched;
		events.TouchpadTouchEnd -= DoTouchpadUntouched;
		events.TouchpadAxisChanged -= DoTouchpadAxisChanged;

		teleportPointer.enabled = true;
	}
}
