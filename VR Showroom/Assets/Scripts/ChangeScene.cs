﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour {

	public string scene;

    public GameObject mainCamera;
    public GameObject spawnPoint;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Q)) {
			SceneManager.LoadScene (scene);
		}

        if (Input.GetKeyDown(KeyCode.E))
        {
            
            if (mainCamera != null)
                mainCamera.transform.position = spawnPoint.transform.position;
        }
    }
}
