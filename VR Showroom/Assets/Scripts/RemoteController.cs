﻿using UnityEngine;
using System.Collections;
using VRTK;
public class RemoteController : MonoBehaviour
{
	public Transform playButton;
	public Transform pauseButton;
	public Transform forwardButton;
	public Transform rewindButton;
    public MoviePlayer moviePlayer;

    public float buttonDepression = 0;

    private bool buttonPressed = false;

    Button previousButton = Button.None;
    Button currentButton = Button.None;

    private bool active = false;
    private bool lastActive = false;

	float buttonZPositionDefault;
	float buttonZPositionPressed;

	enum Button
	{
		None,
		Play,
		Pause,
		Forward,
		Rewind
	};

    // Use this for initialization
    void Start()
    {
        if (GetComponent<VRTK_ControllerEvents>() == null)
        {
            Debug.LogError("RemoteController is required to be attached to a SteamVR Controller that has the SteamVR_ControllerEvents script attached to it");
            return;
        }

		// Set up event handling
        GetComponent<VRTK_ControllerEvents>().TouchpadPressed += new ControllerInteractionEventHandler(DoTouchpadClicked);
		GetComponent<VRTK_ControllerEvents>().TouchpadReleased += new ControllerInteractionEventHandler(DoTouchpadUnclicked);
		GetComponent<VRTK_ControllerEvents>().TouchpadTouchStart += new ControllerInteractionEventHandler(DoTouchpadTouched);
		GetComponent<VRTK_ControllerEvents>().TouchpadAxisChanged += new ControllerInteractionEventHandler(DoTouchpadAxisChanged);

		buttonZPositionDefault = playButton.localPosition.z;
		buttonZPositionPressed = buttonZPositionDefault - buttonDepression;

    }

    void Update()
    {
        if (active != lastActive)
        {
            // We are hacking this in the BezierPointer script for now
            //GetComponent<BezierPointer>().enabled = !active;
            GetComponentInParent<HiqTeleport>().enabled = !active;

            GetComponent<BezierPointer>().enableTeleport = !active;

            lastActive = active;
        }
    }

    public bool isRemoteActive()
    {
        return active;
    }
	
    public void SetActive(bool enable)
    {
        active = enable;
    }

    void DoTouchpadClicked(object sender, ControllerInteractionEventArgs e)
    {
        if (active)
        {
            Color highlightColor = new Color(0.0f, 1.0f, 0.0f, 1.0f);
            switch (currentButton)
            {
                case Button.Play:
					ChangeColor(playButton, highlightColor, buttonZPositionPressed);
                    break;
                case Button.Pause:
					ChangeColor(pauseButton, highlightColor, buttonZPositionPressed);
                    break;
                case Button.Forward:
					ChangeColor(forwardButton, highlightColor, buttonZPositionPressed);
                    break;
                case Button.Rewind:
					ChangeColor(rewindButton, highlightColor, buttonZPositionPressed);
                    break;
            }
        }

        buttonPressed = true;
    }

    void DoTouchpadUnclicked(object sender, ControllerInteractionEventArgs e)
    {
        if (active)
        {
            Color highlightColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
            switch (currentButton)
            {
                case Button.Play:
					ChangeColor(playButton, highlightColor, buttonZPositionDefault);
                    moviePlayer.play();
                    break;
                case Button.Pause:
					ChangeColor(pauseButton, highlightColor, buttonZPositionDefault);
                    moviePlayer.pause();
                    break;
                case Button.Forward:
					ChangeColor(forwardButton, highlightColor, buttonZPositionDefault);
                    moviePlayer.forward();
                    moviePlayer.play();
                    break;
                case Button.Rewind:
					ChangeColor(rewindButton, highlightColor, buttonZPositionDefault);
                    moviePlayer.rewind();
                    moviePlayer.play();
                    break;
            }
        }

        buttonPressed = false;
    }

    void handleButtons(ControllerInteractionEventArgs e)
    {
        if (active)
        {
            Vector2 axis = e.touchpadAxis;


            float xPrim = axis.x;
            float yPrim = axis.y;

			// 45 degree sin and cos
            float rotation = 0.707106781186547524f;
            /*
            Rotate coordinates 45 degrees
            new_x = old_x * cos(alpha) - old_y * sin(alpha)
            new_y = old_x * sin(alpha) + old_y * cos(alpha)
            */
            float x = xPrim * rotation - yPrim * rotation;
            float y = xPrim * rotation + yPrim * rotation;

            currentButton = Button.None;

            float treshold = 0.15f;

            if (x > treshold && y > treshold)
                currentButton = Button.Forward;
            else if (x > treshold && y <= -treshold)
                currentButton = Button.Pause;
            else if (x <= -treshold && y <= -treshold)
                currentButton = Button.Rewind;
            else if (x <= -treshold && y > treshold)
                currentButton = Button.Play;

            Color standardColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
            switch (previousButton)
            {
                case Button.Play:
                    ChangeColor(playButton, standardColor, playButton.localPosition.z);
                    break;
                case Button.Pause:
                    ChangeColor(pauseButton, standardColor, pauseButton.localPosition.z);
                    break;
                case Button.Forward:
                    ChangeColor(forwardButton, standardColor, forwardButton.localPosition.z);
                    break;
                case Button.Rewind:
                    ChangeColor(rewindButton, standardColor, rewindButton.localPosition.z);
                    break;
            }

            Color highlightColor;
            if (buttonPressed)
                highlightColor = new Color(0.0f, 1.0f, 0.0f, 1.0f);
            else
                highlightColor = new Color(1.0f, 1.0f, 0.0f, 1.0f);

            switch (currentButton)
            {
                case Button.Play:
                    ChangeColor(playButton, highlightColor, playButton.localPosition.z);
                    break;
                case Button.Pause:
                    ChangeColor(pauseButton, highlightColor, pauseButton.localPosition.z);
                    break;
                case Button.Forward:
                    ChangeColor(forwardButton, highlightColor, forwardButton.localPosition.z);
                    break;
                case Button.Rewind:
                    ChangeColor(rewindButton, highlightColor, rewindButton.localPosition.z);
                    break;
            }

            previousButton = currentButton;
        }
    }

    void DoTouchpadTouched(object sender, ControllerInteractionEventArgs e)
    {
        handleButtons(e);
    }

	void DoTouchpadAxisChanged(object sender, ControllerInteractionEventArgs e)
	{
		handleButtons(e);
	}

    void ChangeColor(Transform button, Color color, float zPosition)
    {
        Renderer device = button.gameObject.GetComponent<Renderer>();

        if (device.material.HasProperty("_Color"))
        {
			device.material.SetColor ("_Color", color);
			Vector3 pos = button.localPosition;
			pos.z = zPosition;
			button.localPosition = pos;
        }
    }
}
