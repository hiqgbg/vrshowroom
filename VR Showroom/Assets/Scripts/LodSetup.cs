﻿using UnityEngine;
using System.Collections;

public class LodSetup : MonoBehaviour {

	public GameObject lod0;
	public GameObject lod1;

	// Use this for initialization
	void Start () {

		LOD[] lods = new LOD[3];

		lods[0] = new LOD(0.7f, lod0.GetComponentsInChildren<Renderer>());
		lods[1] = new LOD(1.0f / 4.0f, lod0.GetComponentsInChildren<Renderer>());
		lods[2] = new LOD(0.001f, lod1.GetComponentsInChildren<Renderer>());

		GetComponent<LODGroup> ().SetLODs(lods);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
