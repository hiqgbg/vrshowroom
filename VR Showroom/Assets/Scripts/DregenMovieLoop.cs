﻿using UnityEngine;
using System.Collections;

public class DregenMovieLoop : MonoBehaviour {
    
	public MovieTexture[] movies;

    private AudioSource m_audioSource;
    private Material m_material;
	private int m_current = 0;

	// Use this for initialization
	void Start () {
        m_audioSource = GetComponent<AudioSource>();
		m_material = GetComponent<MeshRenderer> ().material;
		m_material.mainTexture = movies [m_current];
		movies [m_current].Play ();
        m_audioSource.clip = movies[m_current].audioClip;
        m_audioSource.Play();
    }
	
	// Update is called once per frame
	void Update () {

        if (m_current >= movies.Length)
        {
            GetComponent<SteamVR_LoadLevel>().enabled = true;
            return;
        }

        if (!movies[m_current].isPlaying)
        {
            m_current++;
            if (m_current >= movies.Length)
            {
                GetComponent<SteamVR_LoadLevel>().enabled = true;
                return;
            }
            m_material.mainTexture = movies[m_current];
            movies[m_current].Play();
            m_audioSource.clip = movies[m_current].audioClip;
            m_audioSource.Play();
        }

	}
}
