﻿using UnityEngine;
using System.Collections;

public class BezierPointer : VRTK.VRTK_WorldPointer {

	public Material curveMaterial;
	/// The distance to the first control point for the bezier curve.
	public float pointerLength = 10;
	public float pointerCursorRadius = 0.5f;

	private Transform m_curveContainer;
	private VRTK.CurveGenerator m_curveGenerator;
	private LineRenderer m_renderer;
	private Vector3[] m_positions;

	private Vector3 m_controlPoint1;
	private Vector3 m_controlPoint2;
	private Vector3 m_controlPoint3;
	private Vector3 m_hitPosition;
	private Vector3 m_teleportPosition;
	private bool m_aimingAtTrigger = false;
	private Transform m_trigger;

	private GameObject m_pointerCursor;

	// Use this for initialization
	protected override void Start () {
		base.Start();
		InitPointer ();
	}

	// Update is called once per frame
	protected override void Update () {

        if (GetComponent<RemoteController>().isRemoteActive() || !enabled)
        {
            return;
        }

        base.Update ();

		if (!m_curveContainer.gameObject.activeSelf)
			return;

		CalculateControlPoints ();
		UpdateCurve ();
		RaycastCurve ();
		UpdateLine ();
	}

	protected override void InitPointer()
	{
		base.InitPointer ();
		InitCurve();
		InitCursor();
	}

	private void InitCurve()
	{
		GameObject curveContainerObject = new GameObject(string.Format("[{0}]PlayerObject_WorldPointer_BezierPointer_CurvedBeamContainer", this.gameObject.name));
		curveContainerObject.SetActive(false);
		m_curveContainer = curveContainerObject.transform;
		m_curveContainer.parent = null;

		GameObject curvePoint = new GameObject("curvePoint");
		m_curveGenerator = curveContainerObject.AddComponent<VRTK.CurveGenerator>();
		m_curveGenerator.Create(10, 1, curvePoint);
		Destroy(curvePoint);

		// Line rendering
		m_positions = new Vector3[transform.childCount];

		m_renderer = curveContainerObject.AddComponent<LineRenderer>();
		m_renderer.SetWidth(0.05f, 0.05f);
		m_renderer.material = curveMaterial;
		m_renderer.SetVertexCount(transform.childCount);
		m_renderer.motionVectors = false;
		m_renderer.receiveShadows = false;
		m_renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
	}

	private void InitCursor()
	{
		float cursorYOffset = 0.02f;
		m_pointerCursor = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
		m_pointerCursor.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
		m_pointerCursor.GetComponent<MeshRenderer>().receiveShadows = false;
		m_pointerCursor.transform.localScale = new Vector3(pointerCursorRadius, cursorYOffset, pointerCursorRadius);
		m_pointerCursor.name = string.Format("[{0}]PlayerObject_WorldPointer_BezierPointer_PointerCursor", this.gameObject.name);
		m_pointerCursor.SetActive(false);
		Destroy(m_pointerCursor.GetComponent<CapsuleCollider>());
	}

	protected override void TogglePointer(bool state)
	{
        state = state && !GetComponent<RemoteController>().isRemoteActive();
        m_curveContainer.gameObject.SetActive(state);
		m_pointerCursor.SetActive(state);
	}

	protected override void DisablePointerBeam(object sender, VRTK.ControllerInteractionEventArgs e)
	{
		base.PointerSet();
		base.DisablePointerBeam(sender, e);
	}

	protected override void SetPointerMaterial()
	{
		if (m_pointerCursor.GetComponent<MeshRenderer>())
		{
			m_pointerCursor.GetComponent<MeshRenderer>().material = pointerMaterial;
		}

		foreach (MeshRenderer mr in m_pointerCursor.GetComponentsInChildren<MeshRenderer>())
		{
			mr.material = pointerMaterial;
		}

		if (m_pointerCursor.GetComponent<SkinnedMeshRenderer>())
		{
			m_pointerCursor.GetComponent<SkinnedMeshRenderer>().material = pointerMaterial;
		}

		foreach (SkinnedMeshRenderer mr in m_pointerCursor.GetComponentsInChildren<SkinnedMeshRenderer>())
		{
			mr.material = pointerMaterial;
		}

		base.SetPointerMaterial();
	}

	void OnDrawGizmos() {
		Gizmos.color = Color.red;
		Gizmos.DrawLine(transform.position, transform.position + transform.forward * 10000);
		Gizmos.DrawSphere(m_controlPoint1, 0.1f);
		Gizmos.DrawSphere(m_controlPoint2, 0.1f);
		Gizmos.DrawSphere(m_controlPoint3, 0.1f);
		Gizmos.color = Color.yellow;
		Gizmos.DrawSphere (m_hitPosition, 0.1f);
		Gizmos.color = Color.green;
		Gizmos.DrawSphere (m_teleportPosition, 0.1f);
	}

	/// Calculate new controls points for the curve based on the position and rotation of the controller.
	private void CalculateControlPoints()
	{
		Plane floor = new Plane(Vector3.up, Vector3.zero);
		float distanceToFloor;
		if (!floor.Raycast (new Ray (transform.position, transform.forward), out distanceToFloor) || distanceToFloor < 0) {
			distanceToFloor = Mathf.Infinity;
		}

		float actualLength = Mathf.Min (distanceToFloor, pointerLength);

		Vector3 direction = transform.forward;
		direction.y = 0;

		m_controlPoint1 = transform.position + transform.forward * (actualLength / 3);

		// pointing up?
		if (transform.forward.y >= 0)
			m_controlPoint2 = m_controlPoint1 + direction * (actualLength / 3);
		else
			m_controlPoint2 = m_controlPoint1 + transform.forward * (actualLength / 3);

		// always place last control point on ground
		m_controlPoint3 = m_controlPoint2 + direction * (actualLength / 3);
		m_controlPoint3.y = 0;
	}

	/// Updates the curve based on the current control points.
	private void UpdateCurve()
	{
		Vector3[] beamPoints = new Vector3[] 
		{
			this.transform.position,
			m_controlPoint1,
			m_controlPoint2,
			m_controlPoint3,
		};

		m_curveGenerator.SetPoints (beamPoints, null);

		// for some reason curve generator ignore first and last position when
		// we're using custom objects as curve markers
		m_curveContainer.GetChild(0).position = this.transform.position + this.transform.forward * 0.03f;
		m_curveContainer.GetChild (m_curveContainer.childCount-1).position = m_controlPoint3;
	}

	/// Tests if the curve intersects any colliders, and if so disabled the curve points that are after the collision 
	/// and moves the last enabled point to the intersect position.
	private void RaycastCurve()
	{
		m_hitPosition = m_controlPoint3;

		bool hit = false;
		m_aimingAtTrigger = false;
		Vector3 previousPosition = m_curveContainer.GetChild (0).position;
		foreach (Transform child in m_curveContainer) 
		{
			if (hit) 
			{
				child.position = m_hitPosition;
			}
			else 
			{
				Ray ray = new Ray (previousPosition, child.position - previousPosition);
				RaycastHit hitData;
				hit = Physics.Raycast (ray, out hitData, Vector3.Distance (previousPosition, child.position));
				if (hit) 
				{
					if (hitData.transform.tag == "TeleportTrigger") {
						m_aimingAtTrigger = true;
						m_trigger = hitData.transform;
					}
					child.position = hitData.point;
					m_hitPosition = hitData.point;
				}
				previousPosition = child.position;
			}
		}

		if (m_aimingAtTrigger) 
		{
			m_trigger.FindChild("Particle System").gameObject.SetActive(true);
			m_pointerCursor.SetActive (false);
			pointerContactTarget = m_trigger;
		}
		else {
			// reset the trigger if there is a valid one
			if (m_trigger) {
				m_trigger.FindChild("Particle System").gameObject.SetActive(false);
				m_trigger = null;
			}
			// raycast down to floor to get teleport location...

			Vector3 downRayOrigin = Vector3.MoveTowards (m_hitPosition, transform.position, 0.001f);
			downRayOrigin.y = m_hitPosition.y + 0.001f;
			Ray downRay = new Ray (downRayOrigin, Vector3.down);
			RaycastHit downHitData;
			if (Physics.Raycast (downRay, out downHitData) && downHitData.transform.tag == "Floor") {
				m_teleportPosition = downHitData.point;
				m_renderer.material.color = pointerHitColor;
				UpdatePointerMaterial (pointerHitColor);

				pointerContactTarget = downHitData.transform;
				destinationPosition = m_teleportPosition;

				m_pointerCursor.SetActive (true);
				m_pointerCursor.transform.position = destinationPosition;

			} else {
				m_teleportPosition = transform.position;
				m_renderer.material.color = pointerMissColor;
				UpdatePointerMaterial (pointerMissColor);

				pointerContactTarget = null;
				destinationPosition = Vector3.zero;

				m_pointerCursor.SetActive (false);
				m_pointerCursor.transform.position = destinationPosition;
			}
		}
	}

	/// Updates the line renderer to match the enabled curve points.
	private void UpdateLine ()
	{
		if (m_positions.Length != m_curveContainer.childCount) 
		{
			m_positions = new Vector3[m_curveContainer.childCount];
			m_renderer.SetVertexCount (m_curveContainer.childCount);
		}

		int i = 0;
		foreach (Transform child in m_curveContainer) 
		{
			m_positions [i] = child.position;
			i++;
		}

		m_renderer.SetPositions (m_positions);

		if (m_positions.Length >= 2) 
		{
			float distance = Vector3.Distance (m_positions [0], m_positions [1]);
			float textureScale = 30.0f * distance / 3.0f;

			m_renderer.material.mainTextureScale = new Vector2 (textureScale, 1);
			m_renderer.material.mainTextureOffset = new Vector2(-Time.time, 0); 
		}
	}
}
