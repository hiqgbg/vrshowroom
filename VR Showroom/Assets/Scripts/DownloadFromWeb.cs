﻿using UnityEngine;
using System.Collections;
using System.Xml;

public class DownloadFromWeb : MonoBehaviour {

	// Use this for initialization

	public string url = "http://tagtider.net/goteborg-c/";
	//choose how many entries should be listed
	public int numberOfTrains = 5;

	IEnumerator Start () {

		WWW tagtider = new WWW (url);
		yield return tagtider;

		//lets cut out the first table we are looking for, the rest we don't need and 
		//causes trouble when interpreting as XML
		string avgang = tagtider.text;
		int start = avgang.IndexOf ("<table ", 0);
		int length = avgang.IndexOf ("</table>", 0)-start+8;
		avgang = avgang.Substring (start, length);

		//replace &nbsp - causes trouble with XML 
		avgang = avgang.Replace("&nbsp;", " ");

		//convert to XML
		XmlDocument xml = new XmlDocument();
		xml.LoadXml (avgang);

		string fullString = "";

		int count = 0;
		XmlNodeList list = xml.SelectNodes ("/table/tbody/tr");
		foreach (XmlNode node in list)
		{
			XmlNodeList children = node.ChildNodes;

			if (children.Count < 6)
				//something weird, shouldn't happen
				continue;

			//we don't need child no 2 and 6
			string time = children [0].InnerText;
			string destination = children [1].InnerText;
			string track = children [3].InnerText;
			string train = children [4].InnerText;
			string trainType = children [5].InnerText;

			fullString = fullString + time + " / " + destination + " / " + track + " / " + train + " / " + trainType + "\n";

			count++;
			//stop if we reached the desired limit
			if (count == numberOfTrains)
				break;
		}

		GetComponent<TextMesh> ().text = fullString;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
