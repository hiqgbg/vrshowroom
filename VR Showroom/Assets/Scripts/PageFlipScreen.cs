﻿using UnityEngine;
using System.Collections;

public class PageFlipScreen : VRTK.VRTK_InteractableObject {

	[Header("Page Flip Screen", order = 0)]
	public Texture2D[] Pages;

	private Material mMaterial;
	private int mCurrentPage = 0;

	void SetPage(int i)
	{
		if (i >= 0 && i < Pages.Length) {
			GetComponent<Renderer> ().material.mainTexture = Pages [mCurrentPage];
		}
	}

	// Use this for initialization
	protected override void Start () {
		SetPage (mCurrentPage);
		isUsable = true;
		base.Start ();
	}
	
	public override void OnInteractableObjectUsed(VRTK.InteractableObjectEventArgs e) {
		mCurrentPage++;
		if (mCurrentPage >= Pages.Length)
			mCurrentPage = 0;
		SetPage (mCurrentPage);
		base.OnInteractableObjectUsed (e);
	}
}
