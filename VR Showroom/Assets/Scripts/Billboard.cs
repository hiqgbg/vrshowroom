﻿using UnityEngine;
using System.Collections;

public class Billboard : MonoBehaviour
{
	private Transform billboardTransform;
	private Transform target;

	void Update ()
	{
		billboardTransform.LookAt(target);
	}
	void Start ()
	{
		billboardTransform = this.transform; //cache the transform
		target = Camera.main.transform; //cache the transform of the camera
	}
}