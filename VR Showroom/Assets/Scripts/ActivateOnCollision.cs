﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class ActivateOnCollision : MonoBehaviour {

	public List<GameObject> objectsToActivate = null;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter(Collision collision)
	{
		foreach (GameObject go in objectsToActivate) {
			go.SetActive (true);
		}
	}
}
