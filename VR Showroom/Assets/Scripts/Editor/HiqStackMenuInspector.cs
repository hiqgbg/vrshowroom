using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(HiqStackMenu))]
public class HiqStackMenuInspector : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        HiqStackMenu rMenu = (HiqStackMenu)target;
        if (GUILayout.Button("Regenerate Buttons"))
        {
            rMenu.RegenerateButtons();
        }
    }
}