﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.IO;
using System;

public class MaterialConverter : AssetPostprocessor {

	string generatedMaterialsPath = "Assets/Materials/Generated/";
	string variationMaterialPath = "Assets/Materials/ColorVariations/";

	private char separator = Path.DirectorySeparatorChar;

    private char[] invalidCharacters = Path.GetInvalidFileNameChars();

    Material OnAssignMaterialModel(Material material, Renderer renderer)
	{
		removeNumbering(ref material);
        replaceInvalidCharacters(ref material);

		if (material.name == "")
		{
			return handleNoNameMaterial(material);
		}
			
		string materialPath = findMaterialPath(material);

		// Find if there is a material at the material path
		if(AssetDatabase.LoadAssetAtPath(materialPath, typeof(Material)) as Material)
		{
			// if the material exists we use it!
			return AssetDatabase.LoadAssetAtPath(materialPath, typeof(Material)) as Material;
		}

		return createNewMaterial(material);
	}

	void removeNumbering(ref Material material)
	{
		string name = material.name;
		string findDot = @"(\w+)(.)(\d)";
		Regex regexpDot = new Regex(findDot, RegexOptions.IgnoreCase);
		Match dotMatch = regexpDot.Match(name);

		if(dotMatch.Success)
		{
			material.name = name.Split('.')[0];
		}
	}

    void replaceInvalidCharacters(ref Material material)
    {
        foreach (char invalidChar in invalidCharacters)
        {
            material.name = material.name.Replace(invalidChar, '_');
        }
    }

	Material handleNoNameMaterial(Material material)
	{
		string noNamePath = generatedMaterialsPath + "NoName" + ".mat";

		if(AssetDatabase.LoadAssetAtPath(noNamePath, typeof(Material)) as Material)
		{
			return AssetDatabase.LoadAssetAtPath(noNamePath, typeof(Material)) as Material;
		} else
		{
			convertToValveIfNeeded(ref material);
			AssetDatabase.CreateAsset(material, noNamePath);
			return material;
		}
	}

	string findMaterialPath(Material material)
	{
		string materialPath = "";

		// Find the material path if available
		string path = Application.dataPath;
		IEnumerable<string> files = Directory.GetFiles(path + separator + "Materials" + separator, "*.*", SearchOption.AllDirectories);

		foreach (string file in files)
		{
			string[] allParts = file.Split(separator);
			string lastPart = allParts[allParts.Length - 1];

			if (lastPart == material.name + ".mat")
			{
				string[] materialSplit = file.Split(new[] { separator + "Materials" + separator }, StringSplitOptions.None);
				materialPath = "Assets" + separator + "Materials" + separator + materialSplit[materialSplit.Length - 1];
				break;
			}	
		}

		return materialPath;
	}

	string findCustomName(Material material)
	{
		string customName = "";

		// look for the _c_ tag which precedes a custom name
		string pattern = @"(\w+)(_c_)(\w+)";
		Regex regexp = new Regex(pattern, RegexOptions.IgnoreCase);
		Match match = regexp.Match(material.name);

		if(match.Success)
		{
			string matName = material.name;
			string[] matSplit = matName.Split(new[] { "_c_" }, StringSplitOptions.None);
			customName = matSplit[1];
		}

		return customName;
	}

	Material createNewMaterial(Material material)
	{
		convertToValveIfNeeded(ref material);

		// look for the _c tag which denotes a custom material
		string pattern = @"(\w+)(_c)";
		Regex regexp = new Regex(pattern, RegexOptions.IgnoreCase);
		Match match = regexp.Match(material.name);

		if(match.Success)
		{
			return createMaterialFromLibrary(material);
		}
		else // if the material does not exist and has no _c, create it and convert to valveMaterial
		{
			return createGenericMaterial(material);
		}
	}

	Material createMaterialFromLibrary(Material material)
	{
		// Find our library material
		string originalMaterialPath = "Assets" + separator + "Materials" + separator + material.name;
		string[] matSplit = originalMaterialPath.Split(new[] { "_c" }, StringSplitOptions.None);
		originalMaterialPath = matSplit[0] + ".mat";


		Material libraryMaterial = AssetDatabase.LoadAssetAtPath(originalMaterialPath, typeof(Material)) as Material;

		if(libraryMaterial == null)
		{
			return createGenericMaterial(material);
		}
		else
		{
			Material customMaterial = new Material(libraryMaterial);
			customMaterial.color = material.color;
			customMaterial.mainTexture = material.mainTexture;

			string customName = findCustomName(material);
			if(customName == "")
				customName = customMaterial.color.ToString();

			// If such a custom material already exist, we use it.
			string customMaterialPath = variationMaterialPath + libraryMaterial.name + "_" + customName + ".mat";
			if(AssetDatabase.LoadAssetAtPath(customMaterialPath, typeof(Material)) as Material)
			{
				return AssetDatabase.LoadAssetAtPath(customMaterialPath, typeof(Material)) as Material;
			} 

			// Create the custom material
			AssetDatabase.CreateAsset(customMaterial, customMaterialPath);
			return customMaterial;
		}
	}

	void convertToValveIfNeeded(ref Material material)
	{
		if (material.shader.name != "vr_standard")
		{
			List< string > unknownShaders = new List< string >();
			ValveMenuTools.StandardToValveSingleMaterial(material, material.shader, Shader.Find("Valve/vr_standard"), true, unknownShaders);
		}
	}

	Material createGenericMaterial(Material material)
	{
		AssetDatabase.CreateAsset(material, generatedMaterialsPath + material.name + ".mat");
		return material;
	}
}
