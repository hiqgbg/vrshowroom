﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;




public class DuplicateKeyComparer<TKey> : IComparer<TKey> where TKey : IComparable
{
	public int Compare(TKey x, TKey y)
	{
		int result = x.CompareTo (y);

		if (result == 0)
			return 1;   // Handle equality as beeing greater
		else
			return result;
	}
}


public class TriangleHighscore : MonoBehaviour {
	
	public static string GetGameObjectPath(GameObject obj)
	{
		string path = "/" + obj.name;
		while (obj.transform.parent != null)
		{
			obj = obj.transform.parent.gameObject;
			path = "/" + obj.name + path;
		}
		return path;
	}


	[MenuItem ("HiveMenu/Print Triangle Highscore")]
	static void CountVertices()
	{
		SortedList<int, MeshFilter> meshCounter = new SortedList<int, MeshFilter> (new DuplicateKeyComparer<int> ());

		MeshFilter[] mfs = FindObjectsOfType<MeshFilter> ();

		foreach (MeshFilter mf in mfs)
		{
			if (mf.sharedMesh == null) {
				continue;
			}

			int vCount = mf.sharedMesh.vertexCount;

			MeshFilter mfOut;
			if (meshCounter.TryGetValue(vCount, out mfOut))
			{
				if (mfOut.sharedMesh == mf.sharedMesh) {
					continue;
				}
			}

			try{
				meshCounter.Add (vCount, mf);
			}
			catch {
				Debug .Log ("already exists:"+ vCount);
				continue;
			}

			if (meshCounter.Count > 30) {

				meshCounter.RemoveAt (0);
			}
		}

		foreach (KeyValuePair<int, MeshFilter> key in meshCounter)
		{

			Debug.Log ("" + key.Key +": " + GetGameObjectPath(key.Value.gameObject));
		}
	}

	[MenuItem ("HiveMenu/Print Density Highscore")]
	static void CountDensity()
	{
		SortedList<float, MeshFilter> meshCounter = new SortedList<float, MeshFilter>(new DuplicateKeyComparer<float> ());

		MeshFilter[] mfs = FindObjectsOfType<MeshFilter> ();

		foreach (MeshFilter mf in mfs)
		{
			if (mf.sharedMesh == null) {
				continue;
			}

			if (mf.sharedMesh.vertexCount < 1000)
				continue;

			Vector3 size = mf.gameObject.GetComponent<MeshRenderer> ().bounds.size;

			if (size.x != 0 || size.y != 0 || size.z != 0) {
				if (size.x == 0)
					size.x += 0.001f;
				if (size.y == 0)
					size.y += 0.001f;
				if (size.z == 0)
					size.z += 0.001f;
			} 
			else {
				Debug.Log ("zero bounds:" + GetGameObjectPath (mf.gameObject));
				continue;

			}

			float volume = size.x * size.y * size.z;
			int vCount = mf.sharedMesh.vertexCount;

			float density = ((float)vCount) / volume;

			MeshFilter mfOut;
			if (meshCounter.TryGetValue(density, out mfOut))
			{
				if (mfOut.sharedMesh == mf.sharedMesh) {
					continue;
				}
			}

			try{
				meshCounter.Add (density, mf);
			}
			catch {
				Debug .Log ("already exists:"+ density);
				continue;
			}
			
			if (meshCounter.Count > 10) {
				
				meshCounter.RemoveAt (0);
			}
		}

		foreach (KeyValuePair<float, MeshFilter> key in meshCounter)
		{
			
			Debug.Log ("" + key.Value.sharedMesh.vertexCount + " " + key.Key +": " + GetGameObjectPath(key.Value.gameObject));
		}
	}
}
