using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(HiqRadialMenu))]
public class HiqRadialMenuInspector : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        HiqRadialMenu rMenu = (HiqRadialMenu)target;
        if (GUILayout.Button("Regenerate Buttons"))
        {
            rMenu.RegenerateButtons();
        }
    }
}