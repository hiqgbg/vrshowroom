﻿using UnityEngine;
using System.Collections;

public class MaterialSample : VRTK.VRTK_InteractableObject
{
    Vector3 m_originalPosition;
    Quaternion m_orignalRotation;

    public GameObject[] carParts;

    // Use this for initialization
    protected override void Start()
    {
        base.Start();

        m_originalPosition = transform.position;
        m_orignalRotation = transform.rotation;

		if (carParts.Length == 0 || carParts [0] == null)
			Debug.LogWarning ("material sample does not have carparts");
    }

    // Update is called once per frame
    /*protected override void Update()
    {

    }*/

    public override void OnInteractableObjectGrabbed(VRTK.InteractableObjectEventArgs e)
    {
        GetComponent<Rigidbody>().useGravity = true;
        //GetComponent<Rigidbody>().isKinematic = false;
        base.OnInteractableObjectGrabbed(e);
    }

    public override void OnInteractableObjectUngrabbed(VRTK.InteractableObjectEventArgs e)
    {
        base.OnInteractableObjectUngrabbed(e);
    }

    void ResetPosition()
    {
        Rigidbody rb = GetComponent<Rigidbody>();
        rb.useGravity = false;
        //rb.isKinematic = true;
        transform.position = m_originalPosition;
        transform.rotation = m_orignalRotation;
        rb.Sleep();
    }

    void OnCollisionEnter(Collision collision)
    {
		if (carParts.Length == 0 || carParts [0] == null)
			return;
		
		if (carParts[0].transform.IsChildOf(collision.transform))
        {
            /*Material[] mats = GetComponent<MeshRenderer>().materials;
            foreach (GameObject cp in carParts)
            {
                foreach (MeshRenderer mr in cp.GetComponentsInChildren<MeshRenderer>())
                {
                    mr.materials = mats;
                }
            }*/
            Material mat = GetComponent<MeshRenderer>().sharedMaterial;
            foreach (GameObject cp in carParts)
            {
                foreach (MeshRenderer mr in cp.GetComponentsInChildren<MeshRenderer>())
                {
                    Material[] mmm = mr.sharedMaterials;
                    mmm[0] = mat;
                    mr.materials = mmm;
                }

				if (cp.GetComponent<HapticObject>())
					cp.GetComponent<HapticObject> ().BaseStart ();
            }
        }

        if (!IsGrabbed())
        {
            ResetPosition();
        }
    }

    void OnCollisionStay(Collision collision)
    {
        if (!IsGrabbed())
        {
            ResetPosition();
        }
    }
}