﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (Collider))]
public class SlidingDoors : MonoBehaviour {

	public GameObject LeftDoor;
	public GameObject RightDoor;

	public Vector3 axis = new Vector3(1.0f, 0.0f, 0.0f);

	public float OpeningDistance = 1.0f;

	private Vector3 LeftStartLocation;
	private Vector3 RightStartLocation;

	void Start()
	{
		LeftStartLocation = LeftDoor.transform.position;
		RightStartLocation = RightDoor.transform.position;
	}

	void OnTriggerEnter(Collider other) {
		//OpenDoors();
	}

	void OnTriggerExit(Collider other) {
		CloseDoors();
	}

	//IEnumerator OpenDoors(Collider other) {
	//	yield return StartCoroutine(OpenDoorsEvenMoreTemp());
	//}

	void OpenDoorsEvenMoreTemp()
	{
		//Mathf.Lerp (); 

		Vector3 translationLeft = axis * OpeningDistance;
		LeftDoor.transform.Translate(translationLeft);

		Vector3 translationRight = axis * OpeningDistance * (-1.0f);
		RightDoor.transform.Translate(translationRight);
	}

	void CloseDoors()
	{
		Vector3 translationRight = axis * OpeningDistance;
		LeftDoor.transform.Translate(translationRight);

		Vector3 translationLeft = axis * OpeningDistance * (-1.0f);
		RightDoor.transform.Translate(translationLeft);
	}
}
