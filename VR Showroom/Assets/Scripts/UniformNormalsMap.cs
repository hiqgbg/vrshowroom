﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class UniformNormalsMap : MonoBehaviour
{

    public Material metallicMaterial;

    public int size = 1024;

    private Texture2D normalMap;

    // Use this for initialization
    void Start()
    {
        normalMap = new Texture2D(size, size, TextureFormat.RGB24, true, true);
        GenerateNormalMap();


        metallicMaterial.mainTexture = normalMap;
    }

    private void GenerateNormalMap()
    {
        // Generate a good number of uniformly distributed normals.
        List<Vector3> normals = new List<Vector3>();
        normals.Add(Vector3.up);
        normals.Add(Vector3.right);
        normals.Add(Vector3.down);
        normals.Add(Vector3.left);
        normals.Add(Vector3.forward);

        List<int> indices = new List<int>();
        indices.Add(0);
        indices.Add(1);
        indices.Add(4);
        indices.Add(1);
        indices.Add(2);
        indices.Add(4);
        indices.Add(2);
        indices.Add(3);
        indices.Add(4);
        indices.Add(3);
        indices.Add(0);
        indices.Add(4);

        do
        {
            int count = indices.Count;
            for (int i = 0; i < count; i += 3)
            {
                int i1 = indices[i];
                int i2 = indices[i + 1];
                int i3 = indices[i + 2];
                Vector3 n = normals[i1] + normals[i2] + normals[i3];
                int newIndex = normals.Count;
                normals.Add(n.normalized);
                indices.Add(i1);
                indices.Add(i2);
                indices.Add(newIndex);
                indices.Add(i2);
                indices.Add(i3);
                indices.Add(newIndex);
                indices.Add(i3);
                indices.Add(i1);
                indices.Add(newIndex);
            }
            indices.RemoveRange(0, count);

        } while (normals.Count < 1000);

        // Randomize the normals
        for (int i = 0; i < normals.Count - 1; ++i)
        {
            int ri = UnityEngine.Random.Range(i + 1, normals.Count);
            Vector3 tmp = normals[i];
            normals[i] = normals[ri];
            normals[ri] = tmp;
        }

        // Distribute the normals as uniformly as possible over the texture.
        int[] ni = new int[size * size];
        for (int i = 0; i < ni.Length; ++i)
        {
            ni[i] = i;
        }
        for (int i = 0; i < ni.Length - 1; ++i)
        {
            int ri = UnityEngine.Random.Range(i + 1, ni.Length);
            int tmp = ni[i];
            ni[i] = ni[ri];
            ni[ri] = tmp;
        }

        int totalBytes = 0;
        int tSize = size;
        for (int mip = 0; mip < normalMap.mipmapCount; ++mip)
        {
            totalBytes += tSize * tSize;
            tSize = tSize >> 1;
        }
        totalBytes *= 3;

        byte[] rgb = new byte[totalBytes];
        for (int i = 0; i < ni.Length; ++i)
        {
            Vector3 n = normals[i % normals.Count];
            rgb[ni[i] * 3] = (byte)((n.x * 127.0f) + 128.0f);
            rgb[ni[i] * 3 + 1] = (byte)((n.y * 127.0f) + 128.0f);
            rgb[ni[i] * 3 + 2] = (byte)(n.z * 255.0f);
        }

        // Construct mip maps
        int oldSize = size;
        int oldI = 0;
        int pi = size * size * 3;
        for (int mip = 1; mip < normalMap.mipmapCount; ++mip)
        {
            int mipSize = oldSize >> 1;

            int nextI = pi;
            byte[] c1 = new byte[3];
            byte[] c2 = new byte[3];
            byte[] c3 = new byte[3];
            byte[] c4 = new byte[3];
            for (int y = 0; y < mipSize; ++y)
            {
                int oldY = y * 2 * oldSize;
                for (int x = 0; x < mipSize; ++x)
                {
                    int oldX = x * 2;
                    Array.Copy(rgb, oldI + ((oldY + oldX) * 3) , c1, 0, 3);
                    Array.Copy(rgb, oldI + ((oldY + oldX + 1) * 3), c2, 0, 3);
                    Array.Copy(rgb, oldI + ((oldY + oldSize + oldX) * 3), c3, 0, 3);
                    Array.Copy(rgb, oldI + ((oldY + oldSize + oldX + 1) * 3), c4, 0, 3);
                    Vector3 v1 = new Vector3(((float)c1[0] - 128.0f) / 127.0f, ((float)c1[1] - 128.0f) / 127.0f, (float)c1[2] / 255.0f);
                    Vector3 v2 = new Vector3(((float)c2[0] - 128.0f) / 127.0f, ((float)c2[1] - 128.0f) / 127.0f, (float)c2[2] / 255.0f);
                    Vector3 v3 = new Vector3(((float)c3[0] - 128.0f) / 127.0f, ((float)c3[1] - 128.0f) / 127.0f, (float)c3[2] / 255.0f);
                    Vector3 v4 = new Vector3(((float)c4[0] - 128.0f) / 127.0f, ((float)c4[1] - 128.0f) / 127.0f, (float)c4[2] / 255.0f);
                    v1 = (v1 + v2 + v3 + v4).normalized;
                    rgb[pi++] = (byte)((v1.x * 127.0f) + 128.0f);
                    rgb[pi++] = (byte)((v1.y * 127.0f) + 128.0f);
                    rgb[pi++] = (byte)(v1.z * 255.0f);
                }
            }
            oldI = nextI;
            oldSize = mipSize;
        }
        normalMap.LoadRawTextureData(rgb);

        normalMap.Apply(false);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
